/*
 * Heaps is a special balanced binary tree data structure where the root-node
 * is compared with its children and arranged accordingly.
 *
 * Max Heap: If a has child node b, then key(a) >= key(b).
 * Min Heap: If a has child node b, then key(a) <= key(b).
 *
 * @author Galahad
 * @version 1.0
 * @since 2020-2-27
 */
import java.util.LinkedList;

public class Heaps {

    public static void main(String[] args) {
        System.out.println("DSA-Practice (AmazonCorretto) - Tree Structure: Heaps");
        System.out.println("Action: Create a Max Heap and add several elements.");
        MaxHeap maxHeap = new MaxHeap();
        maxHeap.add(100);
        maxHeap.add(90);
        maxHeap.add(110);
        maxHeap.add(150);
        maxHeap.add(200);
        maxHeap.add(30);
        maxHeap.add(70);
        System.out.println("\nOutput Max Heap for verification.");
        maxHeap.display();
        System.out.println("Action: Remove 70.");
        maxHeap.remove(70);
        System.out.println("\nOutput Max Heap for verification.");
        maxHeap.display();
        System.out.println("Action: Remove 200.");
        maxHeap.remove(200);
        System.out.println("\nOutput Max Heap for verification.");
        maxHeap.display();
    }
}

class Node {

    public int data;

    public Node(int data) {
        this.data = data;
    }
}

class MaxHeap {

    private LinkedList<Node> heap;

    public MaxHeap() {
        heap = new LinkedList<Node>();
    }

    void add(int data) {
        System.out.println("Add: " + data);
        Node node = new Node(data);
        if (heap.size() == 0) {
            heap.add(node);
        }
        else {
            heap.add(node);
            System.out.println("Re-balance in process...");
            swap(node);
        }
    }

    void remove(int data) {
        System.out.println("Remove: " + data);
        int dataIndex = -1;
        for (Node node : heap) {
            if (node.data == data) {
                dataIndex = heap.indexOf(node);
                break;
            }
        }

        if (dataIndex == -1) {
            System.out.println("Target not found.");
        }
        else {
            if (dataIndex == heap.size() -1)
                heap.removeLast();
            else {
                heap.set(dataIndex, heap.getLast());
                heap.removeLast();
                flip(0);
            }
        }
    }

    void display() {
        System.out.print("Max Heap:");
        for (Node node : heap) {
            System.out.print(" " + node.data);
        }
        System.out.print("\n");
    }

    private void flip(int index) {
        Node parent = heap.get(index);
        int indexLeft = findLeft(index);
        int indexRight = findRight(index);

        if ((indexLeft < heap.size()) && (indexRight < heap.size())) {
            Node left = heap.get(indexLeft);
            Node right = heap.get(indexRight);

            if (parent.data < left.data || parent.data < right.data) {
                if (left.data > right.data) {
                    heap.set(index, left);
                    heap.set(indexLeft, parent);
                    flip(indexLeft);
                }
                else {
                    heap.set(index, right);
                    heap.set(indexRight, parent);
                    flip(indexRight);
                }
            }
        }
        else if (indexLeft < heap.size()) {
            Node left = heap.get(indexLeft);
            if (parent.data < left.data) {
                heap.set(index, left);
                heap.set(indexLeft, parent);
                flip(indexLeft);
            }
        }
        else if (indexRight < heap.size()) {
            Node right = heap.get(indexRight);
            if (parent.data < right.data) {
                heap.set(index, right);
                heap.set(indexRight, parent);
                flip(indexRight);
            }
        }
    }

    private void swap(Node node) {
        int parentIndex = findParent(heap.indexOf(node));
        int nodeIndex = heap.indexOf(node);
        if (parentIndex == -1)
            System.out.println("Reached root node.");
        else {
            Node parent = heap.get(parentIndex);
            if (node.data > parent.data) {
                heap.set(parentIndex, node);
                heap.set(nodeIndex, parent);
                swap(node);
            }
        }
    }

    private void get(int index) {
        if (index >= heap.size()) {
            System.out.println("Null");
        }
        else {
            System.out.println(heap.get(index));
        }
    }

    private int findLeft(int index) {
        return index * 2 + 1;
    }

    private int findRight(int index) {
        return index * 2 + 2;
    }

    private int findParent(int index) {
        if ((index % 2) == 0) {
            return index / 2 - 1;
        }
        else {
            return index / 2;
        }
    }
}
