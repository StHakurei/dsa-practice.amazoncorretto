/*
 * AVL Tree is is height balancing binary tree. AVL Tree uses balance factor
 * to check the difference of height of the left and right sub-tree is not more
 * than 1, if the difference is more than 1, it will re-balance the tree by
 * some rotation techniques.
 *
 * @author Galahad
 * @version 1.0
 * @since 2020-2-5
 */
import java.lang.Math;
import java.util.Random;

public class AVLTree {

    public static void main(String[] args) {
        System.out.println("DSA-Practice (AmazonCorretto) Tree Data Structure: AVL Tree.");
        Tree object = new Tree();
        System.out.println("Action: Create an AVL Tree and insert elements and the tree will automatic rebalance when it's needed.");
        object.insert(10, true);
        object.insert(20, true);
        object.insert(30, true);
        object.insert(40, true);
        object.insert(50, true);
        object.insert(25, true);
        System.out.print("In-Order Traversal: ");
        object.inOrderTraversal(object.getRoot());
        System.out.print("\nPre-Order Traversal: ");
        object.preOrderTraversal(object.getRoot());
        System.out.print("\nPost-Order Traversal: ");
        object.postOrderTraversal(object.getRoot());

        System.out.println("\n\nAction: Create an AVL Tree and random insert 20 integers.");
        Tree tree = new Tree();
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            tree.insert(random.nextInt(100), false);
        }
        tree.report();
        System.out.print("In-Order Traversal: ");
        object.inOrderTraversal(tree.getRoot());
        System.out.print("\nPre-Order Traversal: ");
        object.preOrderTraversal(tree.getRoot());
        System.out.print("\nPost-Order Traversal: ");
        object.postOrderTraversal(tree.getRoot());
    }
}

class Node {

    public int data;
    public Node left;
    public Node right;

    public Node(int data) {
        this.data = data;
        left = null;
        right = null;
    }
}

class Tree {

    private Node root;
    private int leftHeight;
    private int rightHeight;

    public Tree() {
        root = null;
        leftHeight = 0;
        rightHeight = 0;
    }

    Node getRoot() {
        return root;
    }

    void insert(int data, boolean display) {
        if (display)
            System.out.println("Insert " + data);

        Node node = new Node(data);
        if (root == null) {
            root = node;
        }
        else {
            Node cursor = root;
            if (cursor.data < data)
                rightHeight ++;
            else
                leftHeight ++;

            while (true) {
                if (cursor.data < data) {
                    if (cursor.right == null) {
                        cursor.right = node;
                        break;
                    }
                    else
                        cursor = cursor.right;
                }
                else {
                    if (cursor.left == null) {
                        cursor.left = node;
                        break;
                    }
                    else
                        cursor = cursor.left;
                }
            }
        }

        if (isBalanced()) {
            if (display)
                System.out.println("Tree is balanced, do nothing.");
        }
        else {
            if (display)
                System.out.println("Tree is unbalanced, starting rotation.");
            if (chooesRight()) {
                if (display)
                    System.out.println("Right subtree is overheight, balancing...");
                if (root.right.left != null) {
                    if (display)
                        System.out.println("Right-Left Priority...");
                    rightLeftRotate(root);
                }
                else {
                    if (display)
                        System.out.println("Single left rotation...");
                    leftRotate(root);
                }
            }
            else {
                if (display)
                    System.out.println("Left subtree is overheight, balancing...");
                if (root.left.right != null) {
                    if (display)
                        System.out.println("Left-Right rotation...");
                    leftRightRotate(root);
                }
                else {
                    if (display)
                        System.out.println("Single right rotation...");
                    rightRotate(root);
                }
            }

            if (display)
                System.out.println("Tree is balanced.");
        }
    }

    boolean chooesRight() {
        return rightHeight > leftHeight;
    }

    boolean isBalanced() {
        return Math.abs(leftHeight - rightHeight) <= 1;
    }

    void report() {
        if (Math.abs(leftHeight - rightHeight) > 1) {
            System.out.println("Unbalanced.");
            System.out.println("Left height: " + leftHeight);
            System.out.println("Right height: " + rightHeight);
            if (chooesRight())
                System.out.println("Right subtree is too high.");
            else
                System.out.println("Left subtree is too high.");
        }
        else {
            System.out.println("Balanced.");
            System.out.println("Left height: " + leftHeight);
            System.out.println("Right height: " + rightHeight);
        }
    }

    void leftRotate(Node parent) {
        Node t2 = parent.right.left;
        Node a = parent;
        Node b = parent.right;

        a.right = t2;

        root = b;
        root.left = a;

        leftHeight ++;
        rightHeight --;
    }

    void rightRotate(Node parent) {
        Node t2 = parent.left.right;
        Node a = parent;
        Node b = parent.left;

        a.left = t2;

        root = b;
        root.right = a;

        rightHeight ++;
        leftHeight --;
    }

    void leftRightRotate(Node parent) {
        Node t2 = parent.left.right;
        Node t4 = parent.left.right.left;
        Node b = parent.left;

        parent.left = t2;
        parent.left.left = b;
        parent.left.left.right = t4;

        rightRotate(parent);
    }

    void rightLeftRotate(Node parent) {
        Node b = parent.right;
        Node t2 = parent.right.left;
        Node t4 = parent.right.left.right;

        b.left = t4;
        t2.right = b;
        parent.right = t2;

        leftRotate(parent);
    }

    void inOrderTraversal(Node parent) {
        if (parent.left != null)
            inOrderTraversal(parent.left);

        System.out.print(" " + parent.data);

        if (parent.right != null)
            inOrderTraversal(parent.right);
    }

    void preOrderTraversal(Node parent) {
        System.out.print(" " + parent.data);

        if (parent.left != null)
            preOrderTraversal(parent.left);

        if (parent.right != null)
            preOrderTraversal(parent.right);
    }

    void postOrderTraversal(Node parent) {
        if (parent.left != null)
            postOrderTraversal(parent.left);

        if (parent.right != null)
            postOrderTraversal(parent.right);

        System.out.print(" " + parent.data);
    }
}
