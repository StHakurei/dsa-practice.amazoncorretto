/*
 * Tree (data strcuture) is a kind of Abstract Data Type (ADT) that simulate
 * a hierarchical tree strcuture, it's a special data structure used for
 * data storage purpose, in tree strcuture each node can have maximum of two
 * children. This sample code will implement Binary Search Tree and includes three
 * traversal algorithms: Pre-Order, In-Order, Post-Order.
 *
 * @author Galahad
 * @version 1.0
 * @since 2020-2-3
 */
public class BinarySearchTree {

    public static void main(String[] args) {
        System.out.println("DSA-Practice (AmazonCorretto) - Tree Structure: Binary Search Tree.");
        Tree object = new Tree();
        int[] sample = {27, 14, 35, 10, 19, 31, 42, 9, 11, 20, 18, 30, 32, 41, 43};
        System.out.print("Display sample data:");
        for (int data : sample) {
            System.out.print(" " + data);
        }
        System.out.println("\nCreate a binary search tree and insert these data.");
        for (int data : sample) {
            object.insert(data);
        }
        System.out.println("Action: Search 11 in this tree.");
        object.search(11);
        System.out.println("Action: Search 100 in this tree.");
        object.search(100);
        System.out.println("Action: Search 30 in this tree.");
        object.search(30);

        System.out.print("In-Order Traversal output:");
        object.inOrderTraversal(object.getRoot());
        System.out.print("\nPre-Order Traversal output:");
        object.preOrderTraversal(object.getRoot());
        System.out.print("\nPost-Order Traversal output:");
        object.postOrderTraversal(object.getRoot());
    }
}

class Node {

    public int data;
    public Node leftLeaf;
    public Node rightLeaf;

    public Node(int data) {
        this.data = data;
        leftLeaf = null;
        rightLeaf = null;
    }
}

class Tree {

    private Node root;

    public Tree() {
        root = null;
    }

    Node getRoot() {
        return root;
    }

    void insert(int data) {
        Node node = new Node(data);
        if (root == null) {
            root = node;
        }
        else {
            Node cursor = root;
            while (true) {
                if (cursor.data < data) {
                    if (cursor.rightLeaf != null) {
                        cursor = cursor.rightLeaf;
                    }
                    else {
                        cursor.rightLeaf = node;
                        break;
                    }
                }
                else {
                    if (cursor.leftLeaf != null) {
                        cursor = cursor.leftLeaf;
                    }
                    else {
                        cursor.leftLeaf = node;
                        break;
                    }
                }
            }
        }
    }

    void search(int data) {
        Node cursor = root;
        while (true) {
            if (cursor.data == data) {
                System.out.println("Found: " + data + " in tree structure.");
                break;
            }
            else if (cursor.data < data) {
                if (cursor.rightLeaf != null) {
                    cursor = cursor.rightLeaf;
                }
                else {
                    System.out.println("Not found.");
                    break;
                }
            }
            else {
                if (cursor.leftLeaf != null) {
                    cursor = cursor.leftLeaf;
                }
                else {
                    System.out.println("Not found.");
                    break;
                }
            }
        }
    }

    void inOrderTraversal(Node parent) {
        if (parent.leftLeaf != null)
            inOrderTraversal(parent.leftLeaf);

        System.out.print(" " + parent.data);

        if (parent.rightLeaf != null)
            inOrderTraversal(parent.rightLeaf);
    }

    void preOrderTraversal(Node parent) {
        System.out.print(" " + parent.data);

        if (parent.leftLeaf != null)
            preOrderTraversal(parent.leftLeaf);

        if (parent.rightLeaf != null)
            preOrderTraversal(parent.rightLeaf);
    }

    void postOrderTraversal(Node parent) {
        if (parent.leftLeaf != null)
            postOrderTraversal(parent.leftLeaf);

        if (parent.rightLeaf != null)
            postOrderTraversal(parent.rightLeaf);

        System.out.print(" " + parent.data);
    }
}
