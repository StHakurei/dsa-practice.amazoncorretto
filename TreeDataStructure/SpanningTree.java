/*
 * A Spanning Tree is a subset of Graph which has all the vertices covered with minimum
 * possible number of edges. Hence, a Spanning tree has no cycles and it cannot be disconnected.
 *
 * Mathematical properties of Spanning tree:
 * 1. Spanning Tree has n-1 edges which n is the number of vertices.
 *
 * This sample code will implement two sapnning tree algorithm, they are all greedy algorithm.
 * I - Kruskal's algorithm.
 * II - Prim's algorithm.
 *
 * @author Galahad
 * @version 1.0
 * @since 2020-2-18
 */
import java.util.LinkedList;
import java.util.Collections;
import java.util.Comparator;

public class SpanningTree {

    public static void main(String[] args) {
        System.out.println("DSA-Practice (AmazonCorretto) Tree Data Structure: Spanning Tree.");
        System.out.println("Create a graph with 6 vertices: 'S' 'A' 'B' 'C' 'D' 'T'.");
        Graph object = new Graph();
        object.addVertex("S");
        object.addVertex("C");
        object.addVertex("A");
        object.addVertex("B");
        object.addVertex("D");
        object.addVertex("T");
        System.out.println("Show all vertices.");
        object.showVertices();
        System.out.println("Set adjacency: S - A and cost is 7");
        object.setEdge("S", "A", 7);
        System.out.println("Set adjacency: S - C and cost is 8");
        object.setEdge("S", "C", 8);
        System.out.println("Set adjacency: C - C and cost is 1");
        object.setEdge("C", "C", 1);
        System.out.println("Set adjacency: C - C and cost is 2");
        object.setEdge("C", "C", 2);
        System.out.println("Set adjacency: C - A and cost is 3");
        object.setEdge("C", "A", 3);
        System.out.println("Set adjacency: C - A and cost is 10");
        object.setEdge("C", "A", 10);
        System.out.println("Set adjacency: A - B and cost is 9");
        object.setEdge("A", "B", 2);
        System.out.println("Set adjacency: B - A and cost is 6");
        object.setEdge("B", "A", 6);
        System.out.println("Set adjacency: A - A and cost is 6");
        object.setEdge("A", "B", 6);
        System.out.println("Set adjacency: C - D and cost is 3");
        object.setEdge("C", "D", 3);
        System.out.println("Set adjacency: B - D and cost is 2");
        object.setEdge("B", "D", 2);
        System.out.println("Set adjacency: B - D and cost is 3");
        object.setEdge("B", "D", 3);
        System.out.println("Set adjacency: B - D and cost is 10");
        object.setEdge("B", "D", 10);
        System.out.println("Set adjacency: C - B and cost is 4");
        object.setEdge("C", "B", 4);
        System.out.println("Set adjacency: B - T and cost is 5");
        object.setEdge("B", "T", 5);
        System.out.println("Set adjacency: D - T and cost is 2");
        object.setEdge("D", "T", 2);
        System.out.println("Set adjacency: D - T and cost is 12");
        object.setEdge("D", "T", 12);
        System.out.println("Show all edges.");
        object.showEdges();

        Graph sample1 = Graph.deepCopy(object);
        System.out.println("======================================================");
        System.out.println("Action: Generate Spanning Tree by Kruskal's algorithm.");
        sample1.kruskal();
        System.out.println("Show Spanning Tree edges.");
        sample1.showEdges();

        Graph sample2 = Graph.deepCopy(object);
        System.out.println("======================================================");
        System.out.println("Action: Generate Spanning Tree by Prim's algorithm.");
        sample2.prim();
        System.out.println("Show Spanning Tree edges.");
        sample2.showEdges();
    }
}

class Vertex {
    public String label;
    public boolean visited;

    public Vertex(String label) {
        this.label = label;
        visited = false;
    }

    void show() {
        System.out.println("Vertex: " + label + " Visited: " + visited);
    }
}

class Edge {
    public Vertex from;
    public Vertex to;
    public int cost;

    public Edge(Vertex from, Vertex to, int cost) {
        this.from = from;
        this.to = to;
        this.cost = cost;
    }

    boolean contains(Vertex vertex) {
        return (from.label.equalsIgnoreCase(vertex.label)) || (to.label.equalsIgnoreCase(vertex.label));
    }

    boolean contains(String label) {
        return (from.label.equalsIgnoreCase(label)) || (to.label.equalsIgnoreCase(label));
    }

    boolean isParallel(Edge edge) {
        return (contains(edge.from) && contains(edge.to));
    }

    Vertex move(Vertex vertex) {
        if (vertex.label.equalsIgnoreCase(from.label))
            return to;
        else if (vertex.label.equalsIgnoreCase(to.label))
            return from;
        else
            return null;
    }

    void show() {
        System.out.println(from.label + "<-" + cost + "->" + to.label);
    }
}

class Graph {
    private LinkedList<Vertex> vertices;
    private LinkedList<Edge> edges;
    private boolean circle;

    public Graph() {
        vertices = new LinkedList<Vertex>();
        edges = new LinkedList<Edge>();
        circle = false;
    }

    public static Graph deepCopy(Graph instance) {
        Graph object = new Graph();
        for (Vertex vertex : instance.vertices) {
            object.vertices.add(vertex);
        }
        for (Edge edge : instance.edges) {
            object.edges.add(edge);
        }
        object.circle = false;
        return object;
    }

    void addVertex(String label) {
        Vertex vertex = new Vertex(label);
        if (contains(vertex)) {
            System.out.println("Failed to add new vertex due to deteceted duplication.");
        }
        else {
            vertices.add(vertex);
        }
    }

    void setEdge(String alpha, String omega, int cost) {
        Vertex from = new Vertex(alpha);
        Vertex to = new Vertex(omega);
        if (contains(from) && contains(to)) {
            Edge edge = new Edge(from, to, cost);
            edges.add(edge);
        }
        else {
            System.out.println("Graph doesn't contain one of vertices, failed to add edge.");
        }
    }

    private LinkedList<Edge> findAdjacency(String vertexLabel) {
        LinkedList<Edge> adjacency = new LinkedList<Edge>();
        for (Edge object : edges) {
            if (object.contains(vertexLabel)) {
                adjacency.add(object);
            }
        }
        return adjacency;
    }

    private LinkedList<Edge> findAdjacency(LinkedList<Edge> target, String vertexLabel) {
        LinkedList<Edge> adjacency = new LinkedList<Edge>();
        for (Edge object : target) {
            if (object.contains(vertexLabel)) {
                adjacency.add(object);
            }
        }
        return adjacency;
    }

    private boolean contains(Vertex vertex) {
        for (Vertex object : vertices) {
            if (object.label.equalsIgnoreCase(vertex.label))
                return true;
        }
        return false;
    }

    private boolean contains(String label) {
        for (Vertex object : vertices) {
            if (object.label.equalsIgnoreCase(label))
                return true;
        }
        return false;
    }

    void showVertices() {
        for (Vertex object : vertices) {
            System.out.println("Vertex: " + object.label);
        }
    }

    void showEdges() {
        System.out.println("Edges: " + edges.size());
        for (Edge object : edges) {
            object.show();
        }
    }

    void kruskal() {
        removeLoop();
        removeParrallel();
        System.out.println("Removed loop and parallel edges...");
        sortEdges();
        System.out.println("Sorted edges...");
        System.out.println("Starting build tree...");
        LinkedList<Edge> tree = new LinkedList<Edge>();
        for (Edge edge : edges) {
            System.out.print("\nAnalyze edge ");
            edge.show();
            if (!detectCircle(edge, tree)) {
                System.out.println("Add this edge.");
                tree.add(edge);
            }
            else {
                System.out.println("Detected circle, abandoned.");
            }
            circle = false;
        }
        System.out.println("Replace edges...");
        edges = tree;
        System.out.println("Spanning Tree building completed.");
    }

    private void removeLoop() {
        LinkedList<Edge> list = new LinkedList<Edge>();
        for (Edge object : edges) {
            if (object.from.label.equalsIgnoreCase(object.to.label)) {
                list.add(object);
            }
        }

        for (Edge object : list) {
            edges.remove(object);
        }
    }

    private void removeParrallel() {
        LinkedList<Edge> uniqueList = new LinkedList<Edge>();

        while (edges.size() > 0) {
            LinkedList<Edge> removeList = new LinkedList<Edge>();

            Edge unique = edges.getFirst();
            for (int i = 0; i < edges.size(); i++) {
                Edge edge = edges.get(i);

                if (unique.isParallel(edge)) {
                    if (edge.cost < unique.cost) {
                        unique = edge;
                        removeList.add(edge);
                    }
                    else
                        removeList.add(edge);
                }
            }

            uniqueList.add(unique);

            for (Edge object : removeList) {
                edges.remove(object);
            }
        }

        edges = uniqueList;
    }

    private void sortEdges() {
        int sortIndex = -1;
        while (sortIndex < edges.size() - 2) {
            int tail = sortIndex + 1;
            int next = sortIndex + 2;
            while (edges.get(next).cost < edges.get(tail).cost) {
                Edge temp = edges.get(tail);
                edges.set(tail, edges.get(next));
                edges.set(next, temp);
                if (tail != 0) {
                    tail --;
                    next --;
                }
            }
            sortIndex ++;
        }
    }

    private void sortingEdges(LinkedList<Edge> edges) {
        int sortIndex = -1;
        while (sortIndex < edges.size() - 2) {
            int tail = sortIndex + 1;
            int next = sortIndex + 2;
            while (edges.get(next).cost < edges.get(tail).cost) {
                Edge temp = edges.get(tail);
                edges.set(tail, edges.get(next));
                edges.set(next, temp);
                if (tail != 0) {
                    tail --;
                    next --;
                }
            }
            sortIndex ++;
        }
    }

    private boolean detectCircle(Edge edge, LinkedList<Edge> tree) {
        if (tree.size() == 0)
            return false;

        Vertex a = edge.from;
        Vertex b = edge.to;
        boolean containsA = false;
        boolean containsB = false;

        for (Edge object : tree) {
            if (object.contains(a))
                containsA = true;
            if (object.contains(b))
                containsB = true;
        }

        if (containsA && containsB) {
            LinkedList<Edge> temp = new LinkedList<Edge>();
            for (Edge object : tree) {
                temp.add(object);
            }
            travel(a, temp, b);
            return circle;
        }
        else
            return false;
    }

    private void travel(Vertex from, LinkedList<Edge> paths, Vertex target) {
        LinkedList<Edge> availablePath = paths;
        LinkedList<Edge> needPath = findPath(from, availablePath);
        for (Edge edge : needPath) {
            Vertex newFrom = moving(from, edge);
            if (newFrom.label.equalsIgnoreCase(target.label)) {
                circle = true;
                break;
            }
            else {
                availablePath.remove(edge);
                travel(newFrom, availablePath, target);
            }
        }
    }

    private Vertex moving(Vertex from, Edge edge) {
        return edge.move(from);
    }

    private LinkedList<Edge> findPath(Vertex target, LinkedList<Edge> availablePath) {
        LinkedList<Edge> paths = new LinkedList<Edge>();
        for (Edge edge : availablePath) {
            if (edge.contains(target))
                paths.add(edge);
        }
        return paths;
    }

    void prim() {
        removeLoop();
        removeParrallel();
        System.out.println("Removed loop and parallel edges...");
        LinkedList<Edge> tree = new LinkedList<Edge>();
        LinkedList<String> list = new LinkedList<String>();
        list.add(vertices.getFirst().label);
        while (list.size() < vertices.size()) {
            // Find available path of current vertices in the tree.
            System.out.println("\nFind shortest path for current Spanning Tree vertices...");
            LinkedList<Edge> availablePath = getAvailablePath(list);
            // Pick the shortest edge;
            Edge candidate = getShortest(availablePath);
            // Check this edge is connected to unrecord vertex.
            if (list.contains(candidate.from.label) && list.contains(candidate.to.label)) {
                candidate.show();
                System.out.println("Circle detected, skip.");
            }
            else {
                tree.add(candidate);
                if (!list.contains(candidate.from.label))
                    list.add(candidate.from.label);
                if (!list.contains(candidate.to.label))
                    list.add(candidate.to.label);
                System.out.print("Added edge into tree: ");
                candidate.show();
            }
            // Remove edge from graph.
            edges.remove(candidate);
        }

        System.out.println("Replace edges...");
        edges = tree;
        sortEdges();
        System.out.println("Spanning Tree building completed.");
    }

    private LinkedList<Edge> getAvailablePath(LinkedList<String> vertices) {
        LinkedList<Edge> availablePath = new LinkedList<Edge>();
        for (String vertex : vertices) {
            LinkedList<Edge> paths = findAdjacency(vertex);
            for (Edge edge : paths) {
                availablePath.add(edge);
            }
        }
        return availablePath;
    }

    private Edge getShortest(LinkedList<Edge> availablePath) {
        Edge candidate = null;
        for (Edge edge : availablePath) {
            if (candidate == null)
                candidate = edge;

            if (edge.cost < candidate.cost)
                candidate = edge;
        }
        return candidate;
    }
}
