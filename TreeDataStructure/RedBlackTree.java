/*
 * Red-Black tree is a kind of self-balancing binary search tree.
 * Compared with VAL tree, Red-Black tree has more advantages in frequent insertion
 * or deletion operations, otherwise, VAL Tree is more balanced compared to Red-Black tree.
 *
 * For Red-Balck Tree:
 * - Every node is red or black.
 * - Root is always black.
 * - New insertions are always red.
 * - Every path from root to leaf has the same number of black nodes.
 * - No path can have two consecutive red nodes.
 * - Nulls are considered as black.
 *
 * @author Galahad
 * @version 1.0
 * @since 2020-2-8
 */
import java.util.Random;

public class RedBlackTree {

    public static void main(String[] args) {
        System.out.println("DSA-Practice (AmazonCorretto) Tree Data Structure: Red-Black Tree.");
        System.out.println("Action: Create a Red-Black Tree and insert following integers...");
        Tree object = new Tree();
        object.insert(3, true);
        object.insert(1, true);
        object.insert(5, true);
        object.insert(7, true);
        object.insert(6, true);
        object.insert(8, true);
        object.insert(9, true);
        object.insert(10, true);
        System.out.print("In-Order Traversal: ");
        object.inOrderTraversal(object.getRoot());
        System.out.print("\nPre-Order Traversal: ");
        object.preOrderTraversal(object.getRoot());
        System.out.print("\nPost-Order Traversal: ");
        object.postOrderTraversal(object.getRoot());

        System.out.println("\n\nAction: Create a Red-Black Tree and random insert 20 integers.");
        Tree tree = new Tree();
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            tree.insert(random.nextInt(100), false);
        }
        System.out.print("In-Order Traversal: ");
        object.inOrderTraversal(tree.getRoot());
        System.out.print("\nPre-Order Traversal: ");
        object.preOrderTraversal(tree.getRoot());
        System.out.print("\nPost-Order Traversal: ");
        object.postOrderTraversal(tree.getRoot());
    }
}

enum Color {
    RED, BLACK
}

class Node {

    public int data;
    public Color color;
    public Node parent;
    public Node left;
    public Node right;

    public Node(int data) {
        this.data = data;
        color = Color.RED;
        parent = null;
        left = null;
        right = null;
    }
}

public class Tree {

    private Node root;

    public Tree() {
        root = null;
    }

    Node getRoot() {
        return root;
    }

    void insert(int data, boolean display) {
        if (display)
            System.out.println("Insert: " + data);

        Node node = new Node(data);
        if (root == null) {
            root = node;
        }
        else {
            Node cursor = root;

            while (true) {
                if (cursor.data < data) {
                    if (cursor.right == null) {
                        cursor.right = node;
                        node.parent = cursor;
                        break;
                    }
                    else {
                        cursor = cursor.right;
                    }
                }
                else {
                    if (cursor.left == null) {
                        cursor.left = node;
                        node.parent = cursor;
                        break;
                    }
                    else {
                        cursor = cursor.left;
                    }
                }
            }
        }

        if (display)
            System.out.println("Color check after insertion.");
        colorCheck(node, display);

        if (display)
            System.out.println("Set root color as BLACK.");
        root.color = Color.BLACK;
    }

    void colorCheck(Node node, boolean display) {
        if (!hasGrand(node)) {
            if (display)
                System.out.println("New insert node doesn't have grand node, skip color check.");
        }
        else {
            while (hasGrand(node) && hasRedParent(node)) {
                // Get grand and uncle nodes.
                Node grand = node.parent.parent;
                Node parents = node.parent;
                Node uncle = null;
                if ((grand.left != null) && (grand.left.data == parents.data)) {
                    uncle = grand.right;
                }
                else {
                    uncle = grand.left;
                }

                // Check uncle's color.
                if ((uncle != null) && (uncle.color == Color.RED)) {
                    if (display)
                        System.out.println("Red uncle: Color flip.");
                    colorFlip(grand);
                }
                else {
                    if ((parents.left != null) && (parents.left.data == node.data)) {
                        // Chooes right or right-left rotation.
                        if ((grand.right != null) && (grand.right.data == parents.data)) {
                            if (display)
                                System.out.println("Black uncle: Right-left rotation.");
                            // Do right-left rotation.
                            rightLeftRotate(grand);
                        }
                        else {
                            if (display)
                                System.out.println("Black uncle: Right rotation.");
                            // Do right rotation.
                            rightRotate(grand);
                        }
                    }
                    else {
                        // Chooes left or left-right rotation.
                        if ((grand.left != null) && (grand.left.data == parents.data)) {
                            if (display)
                                System.out.println("Black uncle: Left-right rotation.");
                            // Do left-right rotation.
                            leftRightRotate(grand);
                        }
                        else {
                            if (display)
                                System.out.println("Black uncle: Left rotation.");
                            // Do left rotation.
                            leftRotate(grand);
                        }
                    }
                }

                node = grand;
            }
        }
    }

    boolean hasGrand(Node node) {
        return (node.parent != null) && (node.parent.parent != null);
    }

    boolean hasRedParent(Node node) {
        return (node.color == Color.RED) && (node.parent.color == Color.RED);
    }

    void colorFlip(Node parent) {
        if (parent.color == Color.RED) {
            parent.color = Color.BLACK;
            parent.left.color = Color.RED;
            parent.right.color = Color.RED;
        }
        else {
            parent.color = Color.RED;
            parent.left.color = Color.BLACK;
            parent.right.color = Color.BLACK;
        }
    }

    void leftRotate(Node grand) {
        int tempValue = grand.data;
        Color tempColor = grand.color;

        Node b = grand.right;
        Node c = grand.right.right;
        Node t1 = grand.left;
        Node t2 = b.left;

        grand.data = b.data;
        grand.color = b.color;
        b.data = tempValue;
        b.color = tempColor;

        grand.left = b;
        grand.right = c;
        b.left = t1;
        b.right = t2;

        b.parent = grand;
        c.parent = grand;

        if (t1 != null)
            t1.parent = b;
        if (t2 != null)
            t2.parent = b;

        colorFlip(grand);
    }

    void rightRotate(Node grand) {
        int tempValue = grand.data;
        Color tempColor = grand.color;

        Node b = grand.left;
        Node c = grand.left.left;
        Node t1 = grand.right;
        Node t2 = b.right;

        grand.data = b.data;
        grand.color = b.color;
        b.data = tempValue;
        b.color = tempColor;

        grand.left = c;
        grand.right = b;
        b.left = t2;
        b.right = t1;

        c.parent = grand;
        b.parent = grand;

        if (t1 != null)
            t1.parent = b;
        if (t2 != null)
            t2.parent = b;

        colorFlip(grand);
    }

    void rightLeftRotate(Node grand) {
        int tempValue = grand.data;
        Color tempColor = grand.color;

        Node b = grand.right;
        Node c = grand.right.left;
        Node t1 = grand.left;
        Node t2 = c.left;
        Node t3 = c.right;

        grand.data = c.data;
        grand.color = c.color;
        c.data = tempValue;
        c.color = tempColor;

        grand.left = c;
        grand.right = b;

        c.left = t1;
        c.right = t2;
        b.left = t3;

        b.parent = grand;
        c.parent = grand;

        if (t2 != null)
            t2.parent = c;
        if (t1 != null)
            t1.parent = c;
        if (t3 != null)
            t3.parent = b;

        colorFlip(grand);
    }

    void leftRightRotate(Node grand) {
        int tempValue = grand.data;
        Color tempColor = grand.color;

        Node b = grand.left;
        Node c = grand.left.right;
        Node t1 = grand.right;
        Node t2 = c.left;
        Node t3 = c.right;

        grand.data = c.data;
        grand.color = c.color;
        c.data = tempValue;
        c.color = tempColor;

        grand.left = b;
        grand.right = c;

        b.right = t2;
        c.left = t3;
        c.right = t1;

        b.parent = grand;
        c.parent = grand;

        if (t1 != null)
            t1.parent = c;
        if (t2 != null)
            t2.parent = b;
        if (t3 != null)
            t3.parent = c;

        colorFlip(grand);
    }

    void printNode(Node parent) {
        if (parent.left != null)
            System.out.print(parent.left.color + " <- ");
        else
            System.out.print("null (BLACK) <- ");

        System.out.print(parent.color);

        if (parent.right != null)
            System.out.print(" -> " + parent.right.color + "\n");
        else
            System.out.print(" -> null (BLACK)\n");
    }

    void inOrderTraversal(Node parent) {
        if (parent.left != null)
            inOrderTraversal(parent.left);

        System.out.print(" " + parent.data);

        if (parent.right != null)
            inOrderTraversal(parent.right);
    }

    void preOrderTraversal(Node parent) {
        System.out.print(" " + parent.data);

        if (parent.left != null)
            preOrderTraversal(parent.left);

        if (parent.right != null)
            preOrderTraversal(parent.right);
    }

    void postOrderTraversal(Node parent) {
        if (parent.left != null)
            postOrderTraversal(parent.left);

        if (parent.right != null)
            postOrderTraversal(parent.right);

        System.out.print(" " + parent.data);
    }
}
