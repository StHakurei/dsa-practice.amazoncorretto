/*
 * This sample code will perfrom Queue operations by AmazonCorretto.
 *
 * [Queue Operation]
 * 1. Enqueue - Store element into the queue.
 * 2. Dequeue - Get element from the queue.
 * 3. peek - Check the element at the front of the queue without remove it.
 * 4. isFull - Check if the queue is full.
 * 5. isEmpty - Check if the queue is empty.
 *
 * @author  Galahad
 * @version 1.0
 * @since   2019-12-31
 */
public class BasicOperation {

    public static void main(String[] args) {
        System.out.println("DSA-Practice (AmazonCorretto) - Queue Operations.");
        System.out.println("Create a sample Queue.");
        Queue queue = new Queue();
        System.out.println("Queue size: " + queue.size());
        queue.listAll();
        System.out.println("Action: Add sample data into the queue.");
        String[] sampleData = {"Dell", "AMD", "Airbus", "Google", "Amazon", "GE", "IBM", "Lawson", "Intel"};
        for (String element : sampleData) {
            queue.enqueue(element);
        }
        System.out.println("Queue size: " + queue.size());
        queue.listAll();
        System.out.println("Action: Dequeue element from the queue.");
        System.out.println("Dequeued element: " + queue.dequeue());
        System.out.println("Queue size: " + queue.size());
        queue.listAll();
        System.out.println("Action: Peek element from the queue.");
        System.out.println("Peek element: " + queue.peek());
        System.out.println("Queue size: " + queue.size());
        queue.listAll();
        System.out.println("Action: Dequeue element util the queue is cleared");
        while (queue.size() != 0) {
            System.out.println("Dequeue element: " + queue.dequeue());
            System.out.println("Queue current size: " + queue.size());
        }
        queue.listAll();
    }
}

class Node {

    public String data;
    public Node next;

    public Node(String data) {
        this.data = data;
        this.next = null;
    }
}

class Queue {

    private Node head;
    private Node tail;
    private int size;

    public Queue() {
        head = null;
        tail = null;
        size = 0;
    }

    /**
     * Store element into the queue.
     */
    void enqueue(String data) {
        Node node = new Node(data);
        if (size == 0) {
            node.next = head;
            head = node;
            tail = node;
        }
        else {
            Node cursor = head;
            while (cursor.next != null) {
                cursor = cursor.next;
            }
            cursor.next = node;
            tail = node;
        }
        size ++;
    }

    /**
     * Get element from the queue.
     */
    String dequeue() {
        if (size != 0) {
            String data = head.data;
            Node cursor = head;
            cursor = cursor.next;
            head = cursor;
            size --;
            return data;
        }
        else {
            System.out.println("Error: Queue is empty.");
            return null;
        }
    }

    /**
     * Check the front element.
     */
    String peek() {
        Node node = head;
        return head.data;
    }

    /**
     * Check if the queue is empty.
     */
    boolean isEmpty() {
        return size == 0;
    }

    /**
     * Get size of the queue.
     */
    int size() {
        return size;
    }

    /**
     * Print the whole content of the queue.
     */
    void listAll() {
        Node cursor = head;
        System.out.print("[ (Front) <<< ");
        while (cursor != null) {
            System.out.print(cursor.data + " <<< ");
            cursor = cursor.next;
        }
        System.out.println("(Rear) ]\n");
    }
}
/* Output (100% match)
DSA-Practice (AmazonCorretto) - Queue Operations.
Create a sample Queue.
Queue size: 0
[ (Front) <<< (Rear) ]

Action: Add sample data into the queue.
Queue size: 9
[ (Front) <<< Dell <<< AMD <<< Airbus <<< Google <<< Amazon <<< GE <<< IBM <<< Lawson <<< Intel <<< (Rear) ]

Action: Dequeue element from the queue.
Dequeued element: Dell
Queue size: 8
[ (Front) <<< AMD <<< Airbus <<< Google <<< Amazon <<< GE <<< IBM <<< Lawson <<< Intel <<< (Rear) ]

Action: Peek element from the queue.
Peek element: AMD
Queue size: 8
[ (Front) <<< AMD <<< Airbus <<< Google <<< Amazon <<< GE <<< IBM <<< Lawson <<< Intel <<< (Rear) ]

Action: Dequeue element util the queue is cleared
Dequeue element: AMD
Queue current size: 7
Dequeue element: Airbus
Queue current size: 6
Dequeue element: Google
Queue current size: 5
Dequeue element: Amazon
Queue current size: 4
Dequeue element: GE
Queue current size: 3
Dequeue element: IBM
Queue current size: 2
Dequeue element: Lawson
Queue current size: 1
Dequeue element: Intel
Queue current size: 0
[ (Front) <<< (Rear) ]
*///:~
