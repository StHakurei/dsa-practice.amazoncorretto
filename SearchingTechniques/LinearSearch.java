/*
 * Linear search is very simple search algorithm, this sample code will perform
 * linear search in an array which filled by random integer elements.
 *
 * Method list:
 * contains - check if the array contains target element.
 * search - find target element from a given array and print its index.
 *
 * @author Galahad
 * @version 1.0
 * @since 2020-1-3
 */
import java.util.Random;

public class LinearSearch {

    public static void main(String[] args) {
        Random random = new Random();
        int[] sampleArray = new int[1000];
        System.out.println("DSA-Practice (AmazonCorretto) - Linear Search.");
        System.out.println("Create a sample array contains 1000 integers.");
        for (int i = 0; i < 100; i++) {
            sampleArray[i] = random.nextInt(1000);
        }

        System.out.println("\nAction: Random check if array contains...");
        int count = 0;
        int target;
        while (count < 10) {
            target = random.nextInt(1000);
            System.out.println("Contains: " + target + " - " + contains(sampleArray, target));
            count ++;
        }

        System.out.println("\nAction: Random search target in array...");
        count = 0;
        while (count < 10) {
            target = random.nextInt(1000);
            search(sampleArray, target);
            count ++;
        }
    }

    private static boolean contains(int[] array, int target) {
        for (int element : array) {
            if (element == target) {
                return true;
            }
        }
        return false;
    }

    private static void search(int[] array, int target) {
        boolean result = false;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == target) {
                System.out.println("Found " + target + " at index[" + i + "]");
                result = true;
                break;
            }
        }
        if (!result)
            System.out.println("Not found target " + target);
    }
}
/* Output: (50% match)
DSA-Practice (AmazonCorretto) - Linear Search.
Create a sample array contains 1000 integers.

Action: check if array contains...
Contains: 76 - true
Contains: 381 - true
Contains: 380 - false
Contains: 421 - false
Contains: 726 - false
Contains: 918 - false
Contains: 392 - false
Contains: 188 - false
Contains: 467 - false
Contains: 950 - false

Action: search target in array...
Not found target 698
Not found target 457
Not found target 51
Not found target 4
Not found target 626
Not found target 844
Not found target 107
Not found target 218
Not found target 458
Not found target 397
*///:~
