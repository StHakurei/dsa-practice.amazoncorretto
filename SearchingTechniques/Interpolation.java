/*
 * This sample code will perform Interpolation Search in a sorted array.
 *
 * Method list:
 * contains - Check if the array contains target element.
 * search - find target element from a given array and return its index.
 *
 * @author Galahad
 * @version 1.0
 * @since 2020-1-8
 */
import java.util.Random;

public class InterpolationSearch {

    public static void main(String[] args) {
        System.out.println("DSA-Practice (AmazonCorretto) - Interpolation Search.");
        System.out.println("Create a sorted sample array contains 10 thousand elements and distribute un-equally.");
        Random random = new Random();
        int element = random.nextInt(100);
        int increase = random.nextInt(10);
        int[] sampleArray = new int[10000];
        for (int i = 0; i < sampleArray.length; i++) {
            sampleArray[i] = element;
            element += increase;
        }

        System.out.println("Action: Random check if array contains...");
        int count = 0;
        while (count < 10) {
            int target = random.nextInt(100100);
            System.out.println("Contains " + target + " - " + contains(sampleArray, target));
            count ++;
        }

        System.out.println("\nAction: Random search target in array...");
        count = 0;
        while (count < 10) {
            int target = random.nextInt(100100);
            search(sampleArray, target);
            count ++;
        }
    }

    private static boolean contains(int[] array, int target) {
        if (target < array[0] || target > array[array.length - 1]) {
            return false;
        }
        else {
            int head = 0;
            int probe;
            int tail = array.length - 1;
            while ((tail - head) > 1) {
                probe = head + ((tail - head) / (array[tail] - array[head])) * (target - array[head]);
                if (array[probe] == target) {
                    return true;
                }
                else if (array[probe] > target) {
                    tail = probe - 1;
                }
                else {
                    head = probe + 1;
                }
            }
            return false;
        }
    }

    private static void search(int[] array, int target) {
        if (target < array[0] || target > array[array.length - 1]) {
            System.out.println("Not found target: " + target);
        }
        else {
            int head = 0;
            int probe;
            int tail = array.length - 1;
            while ((tail - head) > 1) {
                probe = head + ((tail - head) / (array[tail] - array[head])) * (target - array[head]);
                if (array[probe] == target) {
                    System.out.println("Found " + target + " at index " + probe);
                    break;
                }
                else if (array[probe] > target) {
                    tail = probe - 1;
                }
                else {
                    head = probe + 1;
                }
            }
            System.out.println("Not found target: " + target);
        }
    }
}
