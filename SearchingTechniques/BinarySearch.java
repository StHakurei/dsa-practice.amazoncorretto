/*
 * This sample code will perform Binary Search in a sorted array.
 *
 * Method list:
 * contains - Check if the array contains target element.
 * search - find target element from a given array and print its index.
 *
 * @author Galahad
 * @version 1.0
 * @since 2020-1-6
 */
import java.util.Random;

public class BinarySearch {

    public static void main(String[] args) {
        System.out.println("DSA-Practice (AmazonCorretto) - Binary Search");
        System.out.println("Create a sample sorted array contains 1000 integers");
        int[] sampleArray = new int[1000];
        int element = 25;
        for (int i = 0; i < 1000; i++) {
            sampleArray[i] = element;
            element += 2;
        }

        Random random = new Random();
        System.out.println("Action: Random check if array contains...");
        int count = 0;
        int target;
        while (count < 10) {
            target = random.nextInt(3000);
            System.out.println("Contains: " + target + " - " + contains(sampleArray, target));
            count ++;
        }

        System.out.println("Action: Random search target in array...");
        count = 0;
        while (count < 10) {
            target = random.nextInt(3000);
            search(sampleArray, target);
            count ++;
        }
    }

    private static boolean contains(int[] array, int target) {
        if (target < array[0] || target > array[array.length - 1]) {
            return false;
        }
        else {
            int head = 0;
            int tail = array.length - 1;
            int middle = head + (tail - head) / 2;
            while (middle != head) {
                if (target > array[middle]) {
                    head = middle;
                    middle = head + (tail - head) / 2;
                }
                else if (target < array[middle]) {
                    tail = middle;
                    middle = head + (tail - head) / 2;
                }
                else {
                    return true;
                }
            }
            return false;
        }
    }

    private static void search(int[] array, int target) {
        if (target < array[0] || target > array[array.length - 1]) {
            System.out.println("Not found target: " + target);
        }
        else {
            int head = 0;
            int tail = array.length - 1;
            int middle = head + (tail - head) / 2;
            while (middle != head) {
                if (target > array[middle]) {
                    head = middle;
                    middle = head + (tail - head) / 2;
                }
                else if (target < array[middle]) {
                    tail = middle;
                    middle = head + (tail - head) / 2;
                }
                else {
                    System.out.println("Found " + target + " at index [" + middle + "]");
                    break;
                }
            }
            System.out.println("Not found target: " + target);
        }
    }
}
