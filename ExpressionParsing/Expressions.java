/*
 * Expression sample code will present notation (arithmetic expression)
 * which include Infix Notation, Postfix Notation, Prefix Notation.
 *
 * Sample notation:
 * Infxi: X+Y
 * Postfix (Reverse Polish): XY+
 * Prefix (Polish): +XY
 *
 * parsePrefix - Parse infix to prefix notation.
 * parsePostfix - Paese infix to postfix notation.
 * evaluate - compute postfix or prefix notation then give result.
 *
 * @author  Galahad
 * @version 1.0
 * @since   2019-12-26
 */
public class Expressions {

    public static void main(String[] args) {
        String sample0 = "1 + 2 + 3 - 4";
        String sample1 = "( 10 + 2 ) * 3";
        String sample2 = "( 1 + 20 ) * ( 4 / 2 )";

        System.out.println("DSA-Practice (AmazonCorretto) - Expression Parsing.");

        Expressions notation0 = new Expressions(sample0);
        System.out.println("\n===================================================");
        System.out.print("Original expression: ");
        notation0.printNotation();
        System.out.print("Postfix parsing: ");
        notation0.parsePostfix();
        notation0.printNotation();
        System.out.println("Evaluate...");
        notation0.evaluate();

        Expressions notation1 = new Expressions(sample0);
        System.out.print("Prefix parsing: ");
        notation1.parsePrefix();
        notation1.printNotation();
        System.out.println("Evaluate...");
        notation1.evaluate();

        Expressions notation2 = new Expressions(sample1);
        System.out.println("\n===================================================");
        System.out.print("Original expression: ");
        notation2.printNotation();
        System.out.print("Postfix parsing: ");
        notation2.parsePostfix();
        notation2.printNotation();
        System.out.println("Evaluate...");
        notation2.evaluate();

        Expressions notation3 = new Expressions(sample1);
        System.out.print("Prefix parsing: ");
        notation3.parsePrefix();
        notation3.printNotation();
        System.out.println("Evaluate...");
        notation3.evaluate();

        Expressions notation4 = new Expressions(sample2);
        System.out.println("\n===================================================");
        System.out.print("Original expression: ");
        notation4.printNotation();
        System.out.print("Postfix parsing: ");
        notation4.parsePostfix();
        notation4.printNotation();
        System.out.println("Evaluate...");
        notation4.evaluate();

        Expressions notation5 = new Expressions(sample2);
        System.out.print("Prefix parsing: ");
        notation5.parsePrefix();
        notation5.printNotation();
        System.out.println("Evaluate...");
        notation5.evaluate();
    }

    private Stack stack;
    private String[] notationList;
    private String notationType = "infix";

    Expressions(String notation) {
        notationList = notation.split(" ");
        stack = new Stack(notationList.length);
    }

    /**
     * Compuate postfix or prefix notation then give result.
     */
    void evaluate() {
        if (notationType.equals("infix")) {
            System.out.println("Notice: Before evaluation, you have to parse expression first.");
        }
        else if (notationType.equals("postfix")) {
            for (String element : notationList) {
                if (element.equals("#"))
                    break;

                if (isOpreator(element)) {
                    String operandA = stack.pop();
                    String operandB = stack.pop();
                    stack.push(compute(operandA, operandB, element));
                }
                else {
                    stack.push(element);
                }
            }
            System.out.println("Result: " + stack.pop());
        }
        else {
            String element = null;
            for (int i = notationList.length - 1; i > 0; i--) {
                element = notationList[i];
                if (element.equals("#"))
                    break;

                if (isOpreator(element)) {
                    String operandA = stack.pop();
                    String operandB = stack.pop();
                    stack.push(compute(operandB, operandA, element));
                }
                else {
                    stack.push(element);
                }
            }
            System.out.println("Result: " + stack.pop());
        }
    }

    String compute(String operandA, String operandB, String operator) {
        int a = Integer.parseInt(operandB);
        int b = Integer.parseInt(operandA);
        switch (operator) {
            case "+":
                return String.valueOf(a + b);
            case "-":
                return String.valueOf(a - b);
            case "*":
                return String.valueOf(a * b);
            case "/":
                return String.valueOf(a / b);
        }
        return null;
    }

    /**
     * Parse infix notation to postfix.
     */
    void parsePostfix() {
        String[] postfix = new String[notationList.length + 1];
        String symbol = null;
        Stack buffer = new Stack(notationList.length);
        buffer.push("#");
        int anker = 0;
        for (int i = 0; i < notationList.length; i++) {
            symbol = notationList[i];
            if (isOpreator(symbol)) {
                if (symbol.equals("(")) {
                    buffer.push(symbol);
                }
                else if (symbol.equals(")")) {
                    while (!buffer.peek().equals("(")) {
                        postfix[anker] = buffer.pop();
                        anker ++;
                    }
                    buffer.pop();
                }
                else {
                    if (getPrecedence(symbol) > getPrecedence(buffer.peek())) {
                        buffer.push(symbol);
                    }
                    else {
                        while (getPrecedence(symbol) <= getPrecedence(buffer.peek())) {
                            postfix[anker] = buffer.pop();
                            anker ++;
                        }
                        buffer.push(symbol);
                    }
                }
            }
            else {
                postfix[anker] = symbol;
                anker ++;
            }
        }

        while (!buffer.isEmpty()) {
            postfix[anker] = buffer.pop();
            anker ++;
        }

        notationList = postfix;
        notationType = "postfix";
    }

    /**
     * Parse infix notation to prefix.
     */
    void parsePrefix() {
        Stack buffer = new Stack(notationList.length + 1);
        Stack operator = new Stack(notationList.length);
        operator.push("#");
        String symbol = null;
        for (int i = notationList.length - 1; i >= 0; i--) {
            symbol = notationList[i];
            if (isOpreator(symbol)) {
                if (symbol.equals(")")) {
                    operator.push(symbol);
                }
                else if (symbol.equals("(")) {
                    while (!operator.peek().equals(")")) {
                        buffer.push(operator.pop());
                    }
                    operator.pop();
                }
                else {
                    if (operator.peek().equals("#")) {
                        operator.push(symbol);
                    }
                    else if (getPrecedence(symbol) >= getPrecedence(operator.peek())) {
                        operator.push(symbol);
                    }
                    else {
                        while (getPrecedence(symbol) < getPrecedence(operator.peek())) {
                            buffer.push(operator.pop());
                        }
                        operator.push(symbol);
                    }
                }
            }
            else {
                buffer.push(symbol);
            }
        }

        while (!operator.isEmpty()) {
            buffer.push(operator.pop());
        }

        String[] prefix = new String[buffer.status()];
        int anker = 0;
        while (!buffer.isEmpty()) {
            prefix[anker] = buffer.pop();
            anker ++;
        }

        notationList = prefix;
        notationType = "prefix";
    }

    void printNotation() {
        if (notationType.equals("postfix")) {
            for (String element : notationList) {
                if (element.equals("#"))
                    break;
                System.out.print(element);
            }
        }
        else {
            for (String element : notationList) {
                if (!element.equals("#"))
                    System.out.print(element);
            }
        }
        System.out.println("");
    }

    boolean isOpreator(String input) {
        switch(input) {
            case "+":
            case "-":
            case "*":
            case "/":
            case "(":
            case ")":
                return true;
        }
        return false;
    }

    int getPrecedence(String input) {
        switch(input) {
            case "+":
            case "-":
                return 2;
            case "*":
            case "/":
                return 3;
        }
        return 1;
    }
}

class Stack {

    private String[] innerList;
    private int topIndex;

    public Stack(int size) {
        innerList = new String[size];
        topIndex = -1;
    }

    /**
     * [Stack Operation]
     * Push - Add element to the top.
     */
    void push(String data) {
        if (!isFull()) {
            topIndex ++;
            innerList[topIndex] = data;
        }
        else {
            System.out.println("Error: Inventory is full.");
        }
    }

    /**
     * [Stack Operation]
     * Pop - Get element from the top.
     */
    String pop() {
        if (!isEmpty()) {
            String data = innerList[topIndex];
            topIndex --;
            return data;
        }
        else {
            System.out.println("Error: Inventory is empty.");
            return null;
        }
    }

    /**
     * [Stack Operation]
     * Peek - Check element from the top.
     */
    String peek() {
        return innerList[topIndex];
    }

    /**
     * Get the size of stack.
     */
    int size() {
        return innerList.length;
    }

    /**
     * Get the current inventory status.
     */
    int status() {
        return topIndex + 1;
    }

    /**
     * [Stack Operation]
     * isFull - Check if stack is full.
     */
    boolean isFull() {
        return topIndex == innerList.length - 1;
    }

    /**
     * [Stack Operation]
     * Peek - Check element from the top.
     */
    boolean isEmpty() {
        return topIndex == -1;
    }
}
/* Output: (100% match)
DSA-Practice (AmazonCorretto) - Expression Parsing.

===================================================
Original expression: 1+2+3-4
Postfix parsing: 12+3+4-
Evaluate...
Result: 2
Prefix parsing: -++1234
Evaluate...
Result: 2

===================================================
Original expression: (10+2)*3
Postfix parsing: 102+3*
Evaluate...
Result: 36
Prefix parsing: *+1023
Evaluate...
Result: 36

===================================================
Original expression: (1+20)*(4/2)
Postfix parsing: 120+42/*
Evaluate...
Result: 42
Prefix parsing: *+120/42
Evaluate...
Result: 42
*///:~
