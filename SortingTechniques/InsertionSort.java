/*
 * Insertion Sort is in-place comparison-based sorting algorithm, it maintaines a sub-list
 * which is always sorted, also it's not suitable for large data sets.
 *
 * @author Galahad
 * @version 1.0
 * @since 2020-1-13
 */
import java.util.Random;

public class InsertionSort {

    public static void main(String[] args) {
        System.out.println("DSA-Practice (AmazonCorretto) - Sorting Techniques: Insertion Sort.");
        System.out.println("Generate an unsorted array contains 100 integers.");
        InsertionSort object = new InsertionSort();
        int[] array100 = object.randomArray(100);
        System.out.println("Show original array.");
        object.print(array100, "Unsorted");
        System.out.println("Execute increase sort.");
        object.print(object.increaseSort(array100), "Sorted");
        System.out.println("Execute decrease sort.");
        object.print(object.decreaseSort(array100), "Sorted");

        System.out.println("Generate an unsorted array contains a thousand integers.");
        int[] array1k = object.randomArray(1000);
        System.out.println("Execute increase sort.");
        object.increaseSort(array1k);
        System.out.println("Execute decrease sort.");
        object.decreaseSort(array1k);

        System.out.println("Generate an unsorted array contains 10 thousand integers.");
        int[] array10k = object.randomArray(10000);
        System.out.println("Execute increase sort.");
        object.increaseSort(array10k);
        System.out.println("Execute decrease sort.");
        object.decreaseSort(array10k);

        System.out.println("Generate an unsorted array contains 100 thousand integers.");
        int[] array100k = object.randomArray(100000);
        System.out.println("Execute increase sort.");
        object.increaseSort(array100k);
        System.out.println("Execute decrease sort.");
        object.decreaseSort(array100k);
    }

    int[] decreaseSort(int[] array) {
        int[] sorted = array.clone();
        int sortIndex = -1;
        double startTime = System.currentTimeMillis();
        while (sortIndex < sorted.length - 2) {
            int tail = sortIndex + 1;
            int next = sortIndex + 2;
            while (sorted[next] > sorted[tail]) {
                int temp = sorted[tail];
                sorted[tail] = sorted[next];
                sorted[next] = temp;
                if (tail != 0) {
                    tail --;
                    next --;
                }
            }
            sortIndex ++;
        }
        double endTime = System.currentTimeMillis();
        System.out.println("Sort finished in " + (endTime - startTime) + " ms");
        return sorted;
    }

    int[] increaseSort(int[] array) {
        int[] sorted = array.clone();
        int sortIndex = -1;
        double startTime = System.currentTimeMillis();
        while (sortIndex < sorted.length - 2) {
            int tail = sortIndex + 1;
            int next = sortIndex + 2;
            while (sorted[next] < sorted[tail]) {
                int temp = sorted[tail];
                sorted[tail] = sorted[next];
                sorted[next] = temp;
                if (tail != 0) {
                    tail --;
                    next --;
                }
            }
            sortIndex ++;
        }
        double endTime = System.currentTimeMillis();
        System.out.println("Sort finished in " + (endTime - startTime) + " ms");
        return sorted;
    }

    int[] randomArray(int length) {
        Random random = new Random();
        int[] unsorted = new int[length];
        for (int i = 0; i < length; i++) {
            unsorted[i] = random.nextInt(length * 2);
        }
        return unsorted;
    }

    void print(int[] array, String label) {
        System.out.print(label + ": [");
        int anker = 0;
        while (anker < array.length) {
            if (anker == array.length - 1) {
                System.out.print(array[anker]);
                break;
            }
            System.out.print(array[anker] + ",");
            anker ++;
        }
        System.out.print("]\n");
    }
}
