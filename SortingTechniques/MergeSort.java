/*
 * Merge sort is a non-in-place and in-comparison sorting algorithm.
 * This algorithm devides the whole list into equal halves unless the atomic values
 * are achieved, then sort sub-list and combine them as one, until the whole list
 * is sorted. Merge sort is based on divide and conquer technique.
 *
 * @author Galahad
 * @version 1.0
 * @since 2020-1-14
 */
import java.util.Random;

public class MergeSort {

    public static void main(String[] args) {
        System.out.println("DSA-Practice (AmazonCorretto) - Sorting Techniques: Merge Sort.");
        System.out.println("Generate an unsorted array contains 100 integers.");
        MergeSort object = new MergeSort();
        int[] array100 = object.randomArray(100);
        System.out.println("Show original array.");
        object.print(array100, "Unsorted");
        System.out.println("Execute increase sort.");
        object.print(object.increaseSort(array100), "Sorted");
        System.out.println("Execute decrease sort.");
        object.print(object.decreaseSort(array100), "Sorted");

        System.out.println("Generate an unsorted array contains a thousand integers.");
        int[] array1k = object.randomArray(1000);
        System.out.println("Execute increase sort.");
        object.increaseSort(array1k);
        System.out.println("Execute decrease sort.");
        object.decreaseSort(array1k);

        System.out.println("Generate an unsorted array contains 10 thousand integers.");
        int[] array10k = object.randomArray(10000);
        System.out.println("Execute increase sort.");
        object.increaseSort(array10k);
        System.out.println("Execute decrease sort.");
        object.decreaseSort(array10k);

        System.out.println("Generate an unsorted array contains 100 thousand integers.");
        int[] array100k = object.randomArray(100000);
        System.out.println("Execute increase sort.");
        object.increaseSort(array100k);
        System.out.println("Execute decrease sort.");
        object.decreaseSort(array100k);

        System.out.println("Generate an unsorted array contains one million integers.");
        int[] array1m = object.randomArray(1000000);
        System.out.println("Execute increase sort.");
        object.increaseSort(array1m);
        System.out.println("Execute decrease sort.");
        object.decreaseSort(array1m);
    }

    int[] increaseSort(int[] array) {
        int[][] divided = new int[array.length][];
        double startTime = System.currentTimeMillis();
        for (int i = 0; i < array.length; i++) {
            int[] temp = new int[1];
            temp[0] = array[i];
            divided[i] = temp;
        }

        int merged = divided[0].length;
        int loopTime = divided.length;
        int remain;
        int count = 0;
        while (merged != array.length) {

            int ankerAlpha = -2;
            int ankerOmega = -1;
            int mergeCount = loopTime / 2;
            remain = loopTime % 2;
            for (int i = 0; i < mergeCount; i++) {
                ankerAlpha += 2;
                ankerOmega += 2;
                int[] temp = increaseMerge(divided[ankerAlpha], divided[ankerOmega]);
                divided[ankerAlpha] = null;
                divided[ankerOmega] = null;
                divided[i] = temp;
            }
            if (remain == 1) {
                divided[mergeCount] = divided[ankerOmega + 1];
                divided[ankerOmega + 1] = null;
            }
            loopTime = mergeCount + remain;
            merged = divided[0].length;
        }
        double endTime = System.currentTimeMillis();
        System.out.println("Sort finished in " + (endTime - startTime) + " ms");
        int[] sorted = new int[divided[0].length];
        for (int i = 0; i < sorted.length; i++) {
            sorted[i] = divided[0][i];
        }
        return sorted;
    }

    int[] increaseMerge(int[] alpha, int[] omega) {
        int[] merged = new int[alpha.length + omega.length];
        int ankerAlpha = 0;
        int ankerOmega = 0;
        for (int i = 0; i < merged.length; i++) {
            if (ankerAlpha == alpha.length) {
                merged[i] = omega[ankerOmega];
                ankerOmega ++;
            }
            else if (ankerOmega == omega.length) {
                merged[i] = alpha[ankerAlpha];
                ankerAlpha ++;
            }
            else {
                if (alpha[ankerAlpha] < omega[ankerOmega]) {
                    merged[i] = alpha[ankerAlpha];
                    ankerAlpha ++;
                }
                else {
                    merged[i] = omega[ankerOmega];
                    ankerOmega ++;
                }
            }
        }
        return merged;
    }

    int[] decreaseSort(int[] array) {
        int[][] divided = new int[array.length][];
        double startTime = System.currentTimeMillis();
        for (int i = 0; i < array.length; i++) {
            int[] temp = new int[1];
            temp[0] = array[i];
            divided[i] = temp;
        }

        int merged = divided[0].length;
        int loopTime = divided.length;
        int remain;
        int count = 0;
        while (merged != array.length) {

            int ankerAlpha = -2;
            int ankerOmega = -1;
            int mergeCount = loopTime / 2;
            remain = loopTime % 2;
            for (int i = 0; i < mergeCount; i++) {
                ankerAlpha += 2;
                ankerOmega += 2;
                int[] temp = decreaseMerge(divided[ankerAlpha], divided[ankerOmega]);
                divided[ankerAlpha] = null;
                divided[ankerOmega] = null;
                divided[i] = temp;
            }
            if (remain == 1) {
                divided[mergeCount] = divided[ankerOmega + 1];
                divided[ankerOmega + 1] = null;
            }
            loopTime = mergeCount + remain;
            merged = divided[0].length;
        }
        double endTime = System.currentTimeMillis();
        System.out.println("Sort finished in " + (endTime - startTime) + " ms");
        int[] sorted = new int[divided[0].length];
        for (int i = 0; i < sorted.length; i++) {
            sorted[i] = divided[0][i];
        }
        return sorted;
    }

    int[] decreaseMerge(int[] alpha, int[] omega) {
        int[] merged = new int[alpha.length + omega.length];
        int ankerAlpha = 0;
        int ankerOmega = 0;
        for (int i = 0; i < merged.length; i++) {
            if (ankerAlpha == alpha.length) {
                merged[i] = omega[ankerOmega];
                ankerOmega ++;
            }
            else if (ankerOmega == omega.length) {
                merged[i] = alpha[ankerAlpha];
                ankerAlpha ++;
            }
            else {
                if (alpha[ankerAlpha] > omega[ankerOmega]) {
                    merged[i] = alpha[ankerAlpha];
                    ankerAlpha ++;
                }
                else {
                    merged[i] = omega[ankerOmega];
                    ankerOmega ++;
                }
            }
        }
        return merged;
    }

    int[] randomArray(int length) {
        Random random = new Random();
        int[] unsorted = new int[length];
        for (int i = 0; i < length; i++) {
            unsorted[i] = random.nextInt(length * 2);
        }
        return unsorted;
    }

    void print(int[] array, String label) {
        System.out.print(label + ": [");
        int anker = 0;
        while (anker < array.length) {
            if (anker == array.length - 1) {
                System.out.print(array[anker]);
                break;
            }
            System.out.print(array[anker] + ",");
            anker ++;
        }
        System.out.print("]\n");
    }
}
