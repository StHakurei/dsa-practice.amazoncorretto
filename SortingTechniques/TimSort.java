/*
 * Tim Sort is a high performance hybrid stable sotring algorithm, it based on
 * Insertion Sort and Merge Sort. Tim Sort has "run size" conception, it will try
 * to divide original list into equal sorted sub-list by Insertion Sort, then use
 * Merge Sort to combine them as one.
 * In this sample, our run size is 64.
 *
 * @author Galahad
 * @version 1.0
 * @since 2020-1-20
 */
import java.util.Random;
import java.util.Arrays;
import java.util.Stack;

public class TimSort {

    public static void main(String[] args) {
        System.out.println("DSA-Practice (AmazonCorretto) - Sorting Techniques: Tim Sort.");
        System.out.println("Generate an unsorted array contains 100 integers.");
        TimSort object = new TimSort();
        int[] array100 = object.randomArray(100);
        System.out.println("Show original array.");
        object.print(array100, "Unsorted");
        System.out.println("Execute increase sort.");
        object.print(object.increaseSort(array100), "Sorted");
        System.out.println("Execute decrease sort.");
        object.print(object.decreaseSort(array100), "Sorted");

        System.out.println("Generate an unsorted array contains a thousand integers.");
        int[] array1k = object.randomArray(1000);
        System.out.println("Execute increase sort.");
        object.increaseSort(array1k);
        System.out.println("Execute decrease sort.");
        object.decreaseSort(array1k);

        System.out.println("Generate an unsorted array contains 10 thousand integers.");
        int[] array10k = object.randomArray(10000);
        System.out.println("Execute increase sort.");
        object.increaseSort(array10k);
        System.out.println("Execute decrease sort.");
        object.decreaseSort(array10k);

        System.out.println("Generate an unsorted array contains 100 thousand integers.");
        int[] array100k = object.randomArray(100000);
        System.out.println("Execute increase sort.");
        object.increaseSort(array100k);
        System.out.println("Execute decrease sort.");
        object.decreaseSort(array100k);

        System.out.println("Generate an unsorted array contains one million integers.");
        int[] array1m = object.randomArray(1000000);
        System.out.println("Execute increase sort.");
        object.increaseSort(array1m);
        System.out.println("Execute decrease sort.");
        object.decreaseSort(array1m);
    }

    private final int MIN_RUNSIZE = 64;

    int[] increaseSort(int[] array) {
        int[] sorted = array.clone();
        double startTime = System.currentTimeMillis();
        /**
         * Divide list into soted sub-lists by minimum run size.
         * If the list is smaller than run size, sort it via insertion sort.
         */
        if (sorted.length <= MIN_RUNSIZE) {
            increaseInsertion(sorted, 0, sorted.length);
        }
        else {
            /**
             * Read element from list until fill the minimum run size,
             * if rest element is not enough to fill the run size, add
             * all rest elements into run size, then execute insertion sort.
             *
             * Record sorted sub-list length, Timsort will maintaine three sorted sub-lists,
             * and merge them as soon as possible.
             *
             * Timsort keeps track the length of these sub-list and make sure their length
             * fill the following two laws.
             * A > B + C *If not, merge B and C.
             * B > C *If not, merge B and C.
             */
            Stack<Integer> stack = new Stack<Integer>();

            int anker = 0;
            while (anker < sorted.length) {

                // Reach the end of list.
                if ((anker + MIN_RUNSIZE) > sorted.length) {
                    increaseInsertion(sorted, anker, sorted.length);
                    stack.push(sorted.length - anker);
                    anker = sorted.length;
                }
                else {
                    increaseInsertion(sorted, anker, anker + MIN_RUNSIZE);
                    stack.push(MIN_RUNSIZE);
                    anker = anker + MIN_RUNSIZE;
                }

                /**
                 * Check if reaches the end of list, then execute final merge,
                 * if not, execute normal merge procedure.
                 */
                if (anker == sorted.length) {
                     int mergedLength = 0;
                     while (mergedLength < sorted.length) {
                         int readTemp = stack.pop();
                         int readAnother = stack.pop();
                         increaseMerge(sorted, anker - (readTemp + readAnother), anker - readTemp, anker);
                         readTemp = readTemp + readAnother;
                         mergedLength = readTemp;
                         stack.push(readTemp);
                     }
                }
                else {
                    int readTemp = stack.pop();
                    while (!stack.empty()) {
                        int readAnother = stack.pop();

                        // Check if B > C;
                        if (readAnother > readTemp) {
                            stack.push(readAnother);
                            break;
                        }
                        else {
                            increaseMerge(sorted, anker - (readTemp + readAnother), anker - readTemp, anker);
                            // Here is execute B + C.
                            readTemp = readTemp + readAnother;
                        }

                        // Check if A > B + C.
                        if (!stack.empty()) {
                            readAnother = stack.pop();
                            if (readAnother > readTemp) {
                                stack.push(readAnother);
                                break;
                            }
                            else {
                                increaseMerge(sorted, anker - (readTemp + readAnother), anker - readTemp, anker);
                                readTemp = readTemp + readAnother;
                            }
                        }
                    }
                    // Re-store the calculate result back to the stack.
                    stack.push(readTemp);
                }
            }
        }

        double endTime = System.currentTimeMillis();
        System.out.println("Sort finished in " + (endTime - startTime) + " ms");
        return sorted;
    }

    void increaseInsertion(int[] array, int start, int length) {
        for (int i = start; i < length - 1; i++) {
            int head = i;
            int read = i + 1;
            while (array[head] > array[read]) {
                int temp = array[read];
                array[read] = array[head];
                array[head] = temp;
                head --;
                read --;
                if (head < start)
                    break;
            }
        }
    }

    void increaseMerge(int[] array, int start, int mid, int end) {
        if (array[mid - 1] <= array[mid]) {
            // Everything is good, do nothing.
        }
        else if (array[end - 1] <= array[start]) {
            int[] runA = Arrays.copyOfRange(array, start, mid);
            int[] runB = Arrays.copyOfRange(array, mid, end);

            for (int i = 0; i < runB.length; i++) {
                array[start] = runB[i];
                start ++;
            }
            for (int i = 0; i < runA.length; i++) {
                array[start] = runA[i];
                start ++;
            }
        }
        else {
            int[] runA = Arrays.copyOfRange(array, start, mid);
            int[] runB = Arrays.copyOfRange(array, mid, end);

            int ankerA = 0;
            int ankerB = 0;

            for (int i = start; i < end; i++) {
                if (ankerA == runA.length) {
                    array[i] = runB[ankerB];
                    ankerB ++;
                }
                else if (ankerB == runB.length) {
                    array[i] = runA[ankerA];
                    ankerA ++;
                }
                else {
                    if (runA[ankerA] < runB[ankerB]) {
                        array[i] = runA[ankerA];
                        ankerA ++;
                    }
                    else {
                        array[i] = runB[ankerB];
                        ankerB ++;
                    }
                }
            }
        }
    }

    int[] decreaseSort(int[] array) {
        int[] sorted = array.clone();
        double startTime = System.currentTimeMillis();
        /**
         * Divide list into soted sub-lists by minimum run size.
         * If the list is smaller than run size, sort it via insertion sort.
         */
        if (sorted.length <= MIN_RUNSIZE) {
            increaseInsertion(sorted, 0, sorted.length);
        }
        else {
            /**
             * Read element from list until fill the minimum run size,
             * if rest element is not enough to fill the run size, add
             * all rest elements into run size, then execute insertion sort.
             *
             * Record sorted sub-list length, Timsort will maintaine three sorted sub-lists,
             * and merge them as soon as possible.
             *
             * Timsort keeps track the length of these sub-list and make sure their length
             * fill the following two laws.
             * A > B + C *If not, merge B and C.
             * B > C *If not, merge B and C.
             */
            Stack<Integer> stack = new Stack<Integer>();

            int anker = 0;
            while (anker < sorted.length) {

                // Reach the end of list.
                if ((anker + MIN_RUNSIZE) > sorted.length) {
                    decreaseInsertion(sorted, anker, sorted.length);
                    stack.push(sorted.length - anker);
                    anker = sorted.length;
                }
                else {
                    decreaseInsertion(sorted, anker, anker + MIN_RUNSIZE);
                    stack.push(MIN_RUNSIZE);
                    anker = anker + MIN_RUNSIZE;
                }

                /**
                 * Check if reaches the end of list, then execute final merge,
                 * if not, execute normal merge procedure.
                 */
                if (anker == sorted.length) {
                     int mergedLength = 0;
                     while (mergedLength < sorted.length) {
                         int readTemp = stack.pop();
                         int readAnother = stack.pop();
                         decreaseMerge(sorted, anker - (readTemp + readAnother), anker - readTemp, anker);
                         readTemp = readTemp + readAnother;
                         mergedLength = readTemp;
                         stack.push(readTemp);
                     }
                }
                else {
                    int readTemp = stack.pop();
                    while (!stack.empty()) {
                        int readAnother = stack.pop();

                        // Check if B > C;
                        if (readAnother > readTemp) {
                            stack.push(readAnother);
                            break;
                        }
                        else {
                            decreaseMerge(sorted, anker - (readTemp + readAnother), anker - readTemp, anker);
                            // Here is execute B + C.
                            readTemp = readTemp + readAnother;
                        }

                        // Check if A > B + C.
                        if (!stack.empty()) {
                            readAnother = stack.pop();
                            if (readAnother > readTemp) {
                                stack.push(readAnother);
                                break;
                            }
                            else {
                                decreaseMerge(sorted, anker - (readTemp + readAnother), anker - readTemp, anker);
                                readTemp = readTemp + readAnother;
                            }
                        }
                    }
                    // Re-store the calculate result back to the stack.
                    stack.push(readTemp);
                }
            }
        }

        double endTime = System.currentTimeMillis();
        System.out.println("Sort finished in " + (endTime - startTime) + " ms");
        return sorted;
    }

    void decreaseInsertion(int[] array, int start, int length) {
        for (int i = start; i < length - 1; i++) {
            int head = i;
            int read = i + 1;
            while (array[head] < array[read]) {
                int temp = array[read];
                array[read] = array[head];
                array[head] = temp;
                head --;
                read --;
                if (head < start)
                    break;
            }
        }
    }

    void decreaseMerge(int[] array, int start, int mid, int end) {
        if (array[mid - 1] >= array[mid]) {
            // Everything is good, do nothing.
        }
        else if (array[end - 1] >= array[start]) {
            int[] runA = Arrays.copyOfRange(array, start, mid);
            int[] runB = Arrays.copyOfRange(array, mid, end);

            for (int i = 0; i < runB.length; i++) {
                array[start] = runB[i];
                start ++;
            }
            for (int i = 0; i < runA.length; i++) {
                array[start] = runA[i];
                start ++;
            }
        }
        else {
            int[] runA = Arrays.copyOfRange(array, start, mid);
            int[] runB = Arrays.copyOfRange(array, mid, end);

            int ankerA = 0;
            int ankerB = 0;

            for (int i = start; i < end; i++) {
                if (ankerA == runA.length) {
                    array[i] = runB[ankerB];
                    ankerB ++;
                }
                else if (ankerB == runB.length) {
                    array[i] = runA[ankerA];
                    ankerA ++;
                }
                else {
                    if (runA[ankerA] > runB[ankerB]) {
                        array[i] = runA[ankerA];
                        ankerA ++;
                    }
                    else {
                        array[i] = runB[ankerB];
                        ankerB ++;
                    }
                }
            }
        }
    }

    int[] randomArray(int length) {
        Random random = new Random();
        int[] unsorted = new int[length];
        for (int i = 0; i < length; i++) {
            unsorted[i] = random.nextInt(length * 2);
        }
        return unsorted;
    }

    void print(int[] array, String label) {
        System.out.print(label + ": [");
        int anker = 0;
        while (anker < array.length) {
            if (anker == array.length - 1) {
                System.out.print(array[anker]);
                break;
            }
            System.out.print(array[anker] + ",");
            anker ++;
        }
        System.out.print("]\n");
    }
}
