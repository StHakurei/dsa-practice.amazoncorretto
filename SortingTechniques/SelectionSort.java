/*
 * Selection sort is a simple in-place comparison-based algorithm which the list
 * divided into two parts, sorted at the left and unsorted at the right. This algorithm
 * is not suitable for large data sets.
 *
 * @author Galahad
 * @version 1.0
 * @since 2020-1-13
 */
import java.util.Random;

public class SelectionSort {

    public static void main(String[] args) {
        System.out.println("DSA-Practice (AmazonCorretto) - Sorting Techniques: Selection Sort.");
        System.out.println("Generate an unsorted array contains 100 integers.");
        SelectionSort object = new SelectionSort();
        int[] array100 = object.randomArray(100);
        System.out.println("Show original array.");
        object.print(array100, "Unsorted");
        System.out.println("Execute increase sort.");
        object.print(object.increaseSort(array100), "Sorted");
        System.out.println("Execute decrease sort.");
        object.print(object.decreaseSort(array100), "Sorted");

        System.out.println("Generate an unsorted array contains a thousand integers.");
        int[] array1k = object.randomArray(1000);
        System.out.println("Execute increase sort.");
        object.increaseSort(array1k);
        System.out.println("Execute decrease sort.");
        object.decreaseSort(array1k);

        System.out.println("Generate an unsorted array contains 10 thousand integers.");
        int[] array10k = object.randomArray(10000);
        System.out.println("Execute increase sort.");
        object.increaseSort(array10k);
        System.out.println("Execute decrease sort.");
        object.decreaseSort(array10k);

        System.out.println("Generate an unsorted array contains 100 thousand integers.");
        int[] array100k = object.randomArray(100000);
        System.out.println("Execute increase sort.");
        object.increaseSort(array100k);
        System.out.println("Execute decrease sort.");
        object.decreaseSort(array100k);
    }

    int[] increaseSort(int[] array) {
        int[] sorted = array.clone();
        double startTime = System.currentTimeMillis();
        for (int i = 0; i < sorted.length; i++) {
            int anker = i;
            int target = sorted[i];
            while (anker < sorted.length - 1) {
                anker ++;
                if (sorted[anker] < target) {
                    int temp = target;
                    target = sorted[anker];
                    sorted[anker] = temp;
                }
            }
            sorted[i] = target;
        }
        double endTime = System.currentTimeMillis();
        System.out.println("Sort finished in " + (endTime - startTime) + " ms");
        return sorted;
    }

    int[] decreaseSort(int[] array) {
        int[] sorted = array.clone();
        double startTime = System.currentTimeMillis();
        for (int i = 0; i < sorted.length; i++) {
            int anker = i;
            int target = sorted[i];
            while (anker < sorted.length - 1) {
                anker ++;
                if (sorted[anker] > target) {
                    int temp = target;
                    target = sorted[anker];
                    sorted[anker] = temp;
                }
            }
            sorted[i] = target;
        }
        double endTime = System.currentTimeMillis();
        System.out.println("Sort finished in " + (endTime - startTime) + " ms");
        return sorted;
    }

    int[] randomArray(int length) {
        Random random = new Random();
        int[] unsorted = new int[length];
        for (int i = 0; i < length; i++) {
            unsorted[i] = random.nextInt(length * 2);
        }
        return unsorted;
    }

    void print(int[] array, String label) {
        System.out.print(label + ": [");
        int anker = 0;
        while (anker < array.length) {
            if (anker == array.length - 1) {
                System.out.print(array[anker]);
                break;
            }
            System.out.print(array[anker] + ",");
            anker ++;
        }
        System.out.print("]\n");
    }
}
