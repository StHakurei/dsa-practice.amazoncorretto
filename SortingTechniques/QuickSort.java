/*
 * Quick sort is a Comparison Based sorting algorithm and based on Divide & Conquer.
 * Quick sort algorithm will take an element as a pivot to divide list into two parts,
 * then find pivot in sub-list and divide it recursively, until the length of sub-list
 * is 1 or 0.
 * How to pick a pivot will siginificantly effect the performance of quick sort.
 * In this sample, program will always take the last element of the list as pivot.
 *
 * @author Galahad
 * @version 1.0
 * @since 2020-1-16
 */
import java.util.Random;

public class QuickSort {

    public static void main(String[] args) {
        System.out.println("DSA-Practice (AmazonCorretto) - Sorting Techniques: Quick Sort.");
        System.out.println("Generate an unsorted array contains 100 integers.");
        QuickSort object = new QuickSort();
        int[] array100 = object.randomArray(100);
        System.out.println("Show original array.");
        object.print(array100, "Unsorted");
        System.out.println("Execute increase sort.");
        object.print(object.increaseSort(array100), "Sorted");
        System.out.println("Execute decrease sort.");
        object.print(object.decreaseSort(array100), "Sorted");

        System.out.println("Generate an unsorted array contains a thousand integers.");
        int[] array1k = object.randomArray(1000);
        System.out.println("Execute increase sort.");
        object.increaseSort(array1k);
        System.out.println("Execute decrease sort.");
        object.decreaseSort(array1k);

        System.out.println("Generate an unsorted array contains 10 thousand integers.");
        int[] array10k = object.randomArray(10000);
        System.out.println("Execute increase sort.");
        object.increaseSort(array10k);
        System.out.println("Execute decrease sort.");
        object.decreaseSort(array10k);

        System.out.println("Generate an unsorted array contains 100 thousand integers.");
        int[] array100k = object.randomArray(100000);
        System.out.println("Execute increase sort.");
        object.increaseSort(array100k);
        System.out.println("Execute decrease sort.");
        object.decreaseSort(array100k);
    }

    int[] increaseSort(int[] array) {
        int[] sorted = array.clone();
        double startTime = System.currentTimeMillis();
        increaseQuickSort(sorted, 0, sorted.length - 1);
        double endTime = System.currentTimeMillis();
        System.out.println("Sort finished in " + (endTime - startTime) + " ms");
        return sorted;
    }

    void increaseQuickSort(int[] array, int lowIndex, int highIndex) {
        if (highIndex > lowIndex) {
            int pivot = increasePartition(array, lowIndex, highIndex);

            increaseQuickSort(array, lowIndex, pivot - 1);
            increaseQuickSort(array, pivot + 1, highIndex);
        }
    }

    int increasePartition(int[] array, int low, int high) {
        if (low > high) {
            return high;
        }
        else {
            int pivot = array[high];
            int[] lowPart = new int[array.length];
            int lowAnker = 0;
            int[] highPart = new int[array.length];
            int highAnker = 0;

            // Divides array into two parts.
            int anker = 0;
            while (anker < high) {
                if (array[anker] <= pivot) {
                    lowPart[lowAnker] = array[anker];
                    lowAnker ++;
                }
                else {
                    highPart[highAnker] = array[anker];
                    highAnker ++;
                }
                anker ++;
            }

            // Combine them as a new array.
            anker = 0;
            for (int i = 0; i < lowAnker; i++) {
                array[anker] = lowPart[i];
                anker ++;
            }

            // Insert pivot into the right place.
            array[anker] = pivot;
            int pivotIndex = anker;
            anker ++;

            for (int i = 0; i < highAnker; i++) {
                array[anker] = highPart[i];
                anker ++;
            }
            return pivotIndex;
        }
    }

    int[] decreaseSort(int[] array) {
        int[] sorted = array.clone();
        double startTime = System.currentTimeMillis();
        decreaseQuickSort(sorted, 0, sorted.length - 1);
        double endTime = System.currentTimeMillis();
        System.out.println("Sort finished in " + (endTime - startTime) + " ms");
        return sorted;
    }

    void decreaseQuickSort(int[] array, int lowIndex, int highIndex) {
        if (highIndex > lowIndex) {
            int pivotIndex = decreasePartition(array, lowIndex, highIndex);

            decreaseQuickSort(array, pivotIndex + 1, highIndex);
            decreaseQuickSort(array, lowIndex, pivotIndex - 1);
        }
    }

    int decreasePartition(int[] array, int low, int high) {
        if (low > high) {
            return high;
        }
        else {
            int pivot = array[high];
            int[] lowPart = new int[array.length];
            int lowAnker = 0;
            int[] highPart = new int[array.length];
            int highAnker = 0;

            // Divides array into two parts.
            int anker = 0;
            while (anker < high) {
                if (array[anker] >= pivot) {
                    highPart[highAnker] = array[anker];
                    highAnker ++;
                }
                else {
                    lowPart[lowAnker] = array[anker];
                    lowAnker ++;
                }
                anker ++;
            }

            // Combine them as a new array.
            anker = 0;
            for (int i = 0; i < highAnker; i++) {
                array[anker] = highPart[i];
                anker ++;
            }

            // Insert pivot into the right place.
            array[anker] = pivot;
            int pivotIndex = anker;
            anker ++;

            for (int i = 0; i < lowAnker; i++) {
                array[anker] = lowPart[i];
                anker ++;
            }
            return pivotIndex;
        }
    }

    int[] randomArray(int length) {
        Random random = new Random();
        int[] unsorted = new int[length];
        for (int i = 0; i < length; i++) {
            unsorted[i] = random.nextInt(length * 2);
        }
        return unsorted;
    }

    void print(int[] array, String label) {
        System.out.print(label + ": [");
        int anker = 0;
        while (anker < array.length) {
            if (anker == array.length - 1) {
                System.out.print(array[anker]);
                break;
            }
            System.out.print(array[anker] + ",");
            anker ++;
        }
        System.out.print("]\n");
    }
}
