/*
 * Bubble sort is a simple sorting algorithm. This sotring algorithm is comparison-based
 * algorithm, it will compare each adjacent elements and swapped them in orider (if they are not).
 * Bubble sort is not suitable for large data sets.
 *
 * @author Galahad
 * @version 1.0
 * @since 2020-1-13
 */
import java.util.Random;

public class BubbleSort {

    public static void main(String[] args) {
        System.out.println("DSA-Practice (AmazonCorretto) - Sorting Techniques: Bubble Sort.");
        System.out.println("Generate an unsorted array contains 100 integers.");
        BubbleSort object = new BubbleSort();
        int[] array100 = object.randomArray(100);
        System.out.println("Show original array.");
        object.print(array100, "Unsorted");
        System.out.println("Execute increase sort.");
        object.print(object.increaseSort(array100), "Sorted");
        System.out.println("Execute decrease sort.");
        object.print(object.decreaseSort(array100), "Sorted");

        System.out.println("Generate an unsorted array contains a thousand integers.");
        int[] array1k = object.randomArray(1000);
        System.out.println("Execute increase sort.");
        object.increaseSort(array1k);
        System.out.println("Execute decrease sort.");
        object.decreaseSort(array1k);

        System.out.println("Generate an unsorted array contains 10 thousand integers.");
        int[] array10k = object.randomArray(10000);
        System.out.println("Execute increase sort.");
        object.increaseSort(array10k);
        System.out.println("Execute decrease sort.");
        object.decreaseSort(array10k);

        System.out.println("Generate an unsorted array contains 100 thousand integers.");
        int[] array100k = object.randomArray(100000);
        System.out.println("Execute increase sort.");
        object.increaseSort(array100k);
        System.out.println("Execute decrease sort.");
        object.decreaseSort(array100k);
    }

    int[] randomArray(int length) {
        Random random = new Random();
        int[] unsorted = new int[length];
        for (int i = 0; i < length; i++) {
            unsorted[i] = random.nextInt(length * 2);
        }
        return unsorted;
    }

    int[] decreaseSort(int[] array) {
        int[] sorted = array.clone();
        // initial bubble index = [indexA, indexB].
        int count = 0;
        int offset = array.length;
        double startTime = System.currentTimeMillis();
        while (count < sorted.length - 1) {
            int indexA = 0;
            int indexB = 1;

            while (indexB < offset) {
                if (sorted[indexA] < sorted[indexB]) {
                    int temp = sorted[indexA];
                    sorted[indexA] = sorted[indexB];
                    sorted[indexB] = temp;
                }
                indexA = indexB;
                indexB ++;
            }
            count ++;
            offset --;
        }
        double endTime = System.currentTimeMillis();
        System.out.println("Sort finished in " + (endTime - startTime) + " ms");
        return sorted;
    }

    int[] increaseSort(int[] array) {
        int[] sorted = array.clone();
        // initial bubble index = [indexA, indexB].
        int count = 0;
        int offset = array.length;
        double startTime = System.currentTimeMillis();
        while (count < sorted.length - 1) {
            int indexA = 0;
            int indexB = 1;

            while (indexB < offset) {
                if (sorted[indexA] > sorted[indexB]) {
                    int temp = sorted[indexA];
                    sorted[indexA] = sorted[indexB];
                    sorted[indexB] = temp;
                }
                indexA = indexB;
                indexB ++;
            }
            count ++;
            offset --;
        }
        double endTime = System.currentTimeMillis();
        System.out.println("Sort finished in " + (endTime - startTime) + " ms");
        return sorted;
    }

    void print(int[] array, String label) {
        System.out.print(label + ": [");
        int anker = 0;
        while (anker < array.length) {
            if (anker == array.length - 1) {
                System.out.print(array[anker]);
                break;
            }
            System.out.print(array[anker] + ",");
            anker ++;
        }
        System.out.print("]\n");
    }
}
