/*
 * This sample code will implement Fibonacci series by recursion algorithm.
 *
 * @author Galahad
 * @version 1.0
 * @since 2020-3-15
 */
public class FibonacciSeries {

    public static void main(String[] args) {
        System.out.println("DSA-Practice (AmazonCorretto) - Fibonacci series.");
        System.out.println("Generate Fibonacci series with length 20.");
        fibonacci(20);
    }

    private static void fibonacci(int length) {
        System.out.print(0 + " " + 1);
        iterator(0, 1, 2, length);
    }

    private static void iterator(int a, int b, int index, int length) {
        if (index < length) {
            System.out.print(" " + (a + b));
            iterator(b, a + b, index + 1, length);
        }
    }
}
