/*
 * This sample code will performs an algorithm to solve the tower of hanoi by
 * recursion.
 *
 * @author Galahad
 * @version 1.0
 * @since 2020-3-15
 */
import java.util.LinkedList;
import java.lang.Math;

public class TowerOfHanoi {

    public static void main(String[] args) {
        System.out.println("DSA-Practice (AmazonCorretto) - Tower of Hanoi.");

        Tower origin = new Tower("Origin");
        Tower auxiliary = new Tower("Auxiliary");
        Tower destination = new Tower("Destination");

        System.out.println("======================================================================================");
        System.out.println("Create three tower: Origin, Auxiliary, Destination. And the Origin contains 3 integers.");
        for (int i = 3; i > 0; i--) {
            origin.put(i);
        }
        System.out.println("Show three towers.");
        origin.display();
        auxiliary.display();
        destination.display();
        System.out.println("\nSolve Tower of Hanoi with recursion algorithm.\n");
        move(3, origin, auxiliary, destination, true);
        System.out.println("\nShow three towers.");
        origin.display();
        auxiliary.display();
        destination.display();

        System.out.println("======================================================================================");
        System.out.println("Re-create origin tower with 5 integers.");
        destination.clear();
        for (int i = 5; i > 0; i--) {
            origin.put(i);
        }
        System.out.println("Show three towers.");
        origin.display();
        auxiliary.display();
        destination.display();
        System.out.println("\nSolve Tower of Hanoi with recursion algorithm.\n");
        move(5, origin, auxiliary, destination, true);
        System.out.println("\nShow three towers.");
        origin.display();
        auxiliary.display();
        destination.display();

        System.out.println("======================================================================================");
        System.out.println("Re-create origin tower with 10 integers.");
        destination.clear();
        for (int i = 10; i > 0; i--) {
            origin.put(i);
        }
        System.out.println("Show three towers.");
        origin.display();
        auxiliary.display();
        destination.display();
        System.out.println("\nSolve Tower of Hanoi with recursion algorithm.\n");
        move(10, origin, auxiliary, destination, false);
        System.out.println("\nShow three towers.");
        origin.display();
        auxiliary.display();
        destination.display();
    }

    private static void move(int height, Tower from, Tower buffer, Tower target, boolean show) {
        if (height == 1) {
            int element = from.get();
            target.put(element);
            if (show)
                System.out.println("-> Move '" + element + "' from " + from.label + " -> " + target.label);
        }
        else {
            move(height - 1, from, target, buffer, show);
            int element = from.get();
            target.put(element);
            if (show)
                System.out.println("-> Move '" + element + "' from " + from.label + " -> " + target.label);
            move(height - 1, buffer, from, target, show);
        }
    }
}

class Tower {

    private LinkedList<Integer> tower;
    public String label;

    public Tower(String label) {
        this.label = label;
        tower = new LinkedList<Integer>();
    }

    public int getHeight() {
        return tower.size();
    }

    public int get() {
        int element = tower.getLast();
        tower.removeLast();
        return element;
    }

    public void put(int element) {
        tower.add(element);
    }

    public void display() {
        System.out.print(label + ": Bottom -| ");
        for (int element : tower) {
            System.out.print(element + " ");
        }
        System.out.println("|- Top");
    }

    public void clear() {
        tower.clear();
    }
}
