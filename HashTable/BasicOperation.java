/*
 * Hash Table (Hash Map) is a kind of Abstract Data Type which store data in an associative manner,
 * Key -> Value, and it has good performance in search operation.
 * Usually Hash Table has "hashcode" function to generate an unique index to mapping key with value,
 * but in collision, multiple keys are pointed to same index, Hash Table has two major ways
 * to solve it, Separate Chaining and Open Addressing.
 *
 * This sample code will implement Hash Table in these two collision solution, and contain following
 * basic methods:
 *
 * search - Search target element in a Hash Table.
 * insert - Insert an element into a Hash Table.
 * remove - Delete an element from a Hash Table.
 *
 * @author Galahad.
 * @version 1.0
 * @since 2020-1-9
 */
import java.util.Random;

public class BasicOperation {

    public static void main(String[] args) {
        System.out.println("DSA-Practice (AmazonCorretto) - HashTable");
        System.out.println("\n=== HashTable (Separate Chaining) ===");
        String[] companies = {"AMD", "Airbus", "Dell", "Intel", "Benz", "Volkswagen", "Amazon", "Google"};
        String[] stockCodes = {"AMDC", "ABUS", "DELL", "INTL", "BENZ", "VKWG", "AMZN", "GOGL"};
        System.out.println("Sample data is 'Company Name' and 'Stock Code'.");
        for (int i = 0; i < companies.length; i++) {
            System.out.println("Company: " + companies[i] + " - [ " + stockCodes[i] + " ]");
        }

        System.out.println("\nCreate HashTable (Separate Chaining) which capacity is 100.");
        SeparateChainTable chainTable = new SeparateChainTable(100);
        System.out.println("Then insert all of data into the table.");
        for (int i = 0; i < companies.length; i++) {
            chainTable.insert(companies[i], stockCodes[i]);
        }
        System.out.println("Stored object: " + chainTable.getStatus());
        chainTable.report(false);
        System.out.println("\nAction: Search following companies and try to get thire stock code.");
        String[] searchSample = {"Google", "Intel", "Lenovo"};
        for (String company : searchSample) {
            System.out.println("Search target company: " + company);
            Bucket result = chainTable.search(company);
            if (result == null)
                System.out.println("False: Missing record about this company.");
            else {
                System.out.println("Found target - Company Name: " + result.objectName + " - Stock Code: " + result.objectId);
            }
        }
        System.out.println("\nAction: Remove Intel from HashTable.");
        chainTable.remove("Intel");
        System.out.println("Stored object: " + chainTable.getStatus());
        chainTable.report(false);
        System.out.println("\nAction: Remove Dell from HashTable.");
        chainTable.remove("Dell");
        System.out.println("Stored object: " + chainTable.getStatus());
        chainTable.report(false);
        System.out.println("\nAction: Add Lenovo and its Stock Code into HashTable.");
        chainTable.insert("Lenovo", "LENO");
        System.out.println("Stored object: " + chainTable.getStatus());
        chainTable.report(false);
        System.out.println("\nAction: Re-search following companies and try to get thire stock code.");
        for (String company : searchSample) {
            System.out.println("Search target company: " + company);
            Bucket result = chainTable.search(company);
            if (result == null)
                System.out.println("False: Missing record about this company.");
            else {
                System.out.println("Found target - Company Name: " + result.objectName + " - Stock Code: " + result.objectId);
            }
        }
        System.out.println("\nAction: Continue add random information util HashTable is full.");
        Random random = new Random();
        while (!chainTable.isFull()) {
            chainTable.insert(randomWord(random.nextInt(20)), randomWord(4));
        }
        System.out.println("Stored object: " + chainTable.getStatus());
        chainTable.report(false);
        System.out.println("\nAction: Remove all of record from HashTable");
        chainTable.removeAll();
        System.out.println("Stored object: " + chainTable.getStatus());
        chainTable.report(false);

        System.out.println("\n=== HashTable (Open Addressing) ===");
        System.out.println("\nCreate HashTable (Open Addressing) which capacity is 50.");
        OpenAddressTable openTable = new OpenAddressTable(50);
        System.out.println("Then insert all of data into the table.");
        for (int i = 0; i < companies.length; i++) {
            openTable.insert(companies[i], stockCodes[i]);
        }
        System.out.println("Stored object: " + openTable.getStatus());
        openTable.report(false);
        System.out.println("\nAction: Search following companies and try to get thire stock code.");
        for (String company : searchSample) {
            System.out.println("Search target company: " + company);
            Entry result = openTable.search(company);
            if (result == null)
                System.out.println("False: Missing record about this company.");
            else {
                System.out.println("Found target - Company Name: " + result.objectName + " - Stock Code: " + result.objectId);
            }
        }
        System.out.println("\nAction: Remove Intel from HashTable.");
        openTable.remove("Intel");
        System.out.println("Stored object: " + openTable.getStatus());
        openTable.report(false);
        System.out.println("\nAction: Remove Dell from HashTable.");
        openTable.remove("Dell");
        System.out.println("Stored object: " + openTable.getStatus());
        openTable.report(false);
        System.out.println("\nAction: Add Lenovo and its Stock Code into HashTable.");
        openTable.insert("Lenovo", "LENO");
        System.out.println("Stored object: " + openTable.getStatus());
        openTable.report(false);
        System.out.println("\nAction: Continue add random information util HashTable is full.");
        while (!openTable.isFull()) {
            openTable.insert(randomWord(random.nextInt(20)), randomWord(4));
        }
        System.out.println("Stored object: " + openTable.getStatus());
        openTable.report(false);
        System.out.println("\nAction: Remove all of record from HashTable");
        openTable.removeAll();
        System.out.println("Stored object: " + openTable.getStatus());
        openTable.report(false);
    }

    private static String randomWord(int length) {
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String word = "";
        Random generator = new Random();
        for (int i = 0; i < length; i++) {
            word += alphabet.charAt(generator.nextInt(alphabet.length() - 1));
        }
        return word;
    }
}

// Separate Chaining solution start from here.
class Bucket {

    public Bucket next;
    public String objectName;
    public String objectId;

    public Bucket(String objectName, String objectId) {
        this.objectName = objectName;
        this.objectId = objectId;
        next = null;
    }
}

class BucketChain {

    private Bucket head;
    private int size;

    public BucketChain() {
        head = null;
        size = 0;
    }

    void add(String objectName, String objectId) {
        Bucket bucket = new Bucket(objectName, objectId);
        bucket.next = head;
        head = bucket;
        size ++;
    }

    void remove(String objectName) {
        Bucket cursor = head;
        Bucket previous = head;
        int anker = 0;
        while (cursor != null) {
            if (cursor.objectName.equals(objectName)) {
                if (anker == 0) {
                    head = cursor.next;
                    cursor.next = null;
                    size --;
                }
                else if (anker == size - 2) {
                    cursor.next = null;
                    size --;
                }
                else {
                    previous.next = cursor.next;
                    size --;
                }
            }
            else {
                previous = cursor;
            }
            cursor = cursor.next;
            anker ++;
        }
    }

    Bucket search(String objectName) {
        Bucket cursor = head;
        while (cursor != null) {
            if (cursor.objectName.equals(objectName)) {
                return cursor;
            }
            cursor = cursor.next;
        }
        return null;
    }

    int getSize() {
        return size;
    }
}

class SeparateChainTable {

    private int status;
    private BucketChain[] hashTable;

    public SeparateChainTable(int size) {
        hashTable = new BucketChain[size];
        status = 0;
    }

    private int hash(String objectName) {
        return objectName.length() % hashTable.length;
    }

    void insert(String objectName, String objectId) {
        if (!isFull()) {
            int index = hash(objectName);
            /**
             * Check if collision happens.
             * null means this slot is ready to use.
             * or collision is happening, store data into the next entry of this chain,
             * but all of data in this chian will be stored at the same table slot.
             */
            if (hashTable[index] == null) {
                BucketChain chain = new BucketChain();
                chain.add(objectName, objectId);
                hashTable[index] = chain;
            }
            else {
                hashTable[index].add(objectName, objectId);
            }
            status ++;
        }
        else {
            System.out.println("Error: Failed to insert new object, table is full.");
        }
    }

    Bucket search(String objectName) {
        int index = hash(objectName);
        if (hashTable[index] == null) {
            return null;
        }
        else {
            return hashTable[index].search(objectName);
        }
    }

    void remove(String objectName) {
        int index = hash(objectName);
        BucketChain targetSlot = hashTable[index];
        if (targetSlot != null && search(objectName) != null) {
            targetSlot.remove(objectName);
            status --;
            if (targetSlot.getSize() == 0)
                hashTable[index] = null;
        }
    }

    void report(boolean showEmpty) {
        System.out.println("HashTable report:");
        if (isEmpty())
            System.out.println("Empty");
        else {
            for (int i = 0; i < hashTable.length; i++) {
                if (hashTable[i] != null) {
                    System.out.println("Slot " + i + " - Used. Object(s): " + hashTable[i].getSize());
                }
                else if (showEmpty){
                    System.out.println("Slot " + i + " - Empyt");
                }
            }
        }
    }

    void removeAll() {
        for (int i = 0; i < hashTable.length; i++) {
            hashTable[i] = null;
            status --;
        }
    }

    int getStatus() {
        return status;
    }

    boolean isEmpty() {
        return status == 0;
    }

    boolean isFull() {
        return status == hashTable.length;
    }
}

// Open Addressing start from here.
class Entry {

    public String objectName;
    public String objectId;

    public Entry(String objectName, String objectId) {
        this.objectName = objectName;
        this.objectId = objectId;
    }
}

class OpenAddressTable {

    private Entry[] hashTable;
    private int status;

    public OpenAddressTable(int size) {
        hashTable = new Entry[size];
        status = 0;
    }

    private int hash(String objectName) {
        return objectName.length() % hashTable.length;
    }

    void insert(String objectName, String objectId) {
        if (isFull()) {
            System.out.println("Error: Failed to insert new object, table is full.");
        }
        else {
            Entry entry = new Entry(objectName, objectId);
            int index = hash(objectName);
            while (true) {
                // If index reaches the length of array, point the index to the head.
                if (index == hashTable.length)
                    index = 0;
                // Find an empty slot to store data, if not, go to the next.
                if (hashTable[index] == null)
                    break;
                else {
                    index ++;
                }
            }
            hashTable[index] = entry;
            status ++;
        }
    }

    void remove(String objectName) {
        int index = hash(objectName);
        int loopMax = 0;
        while (loopMax < hashTable.length) {
            // If index reaches the length of array, point the index to the head.
            if (index == hashTable.length)
                index = 0;
            Entry entry = hashTable[index];
            if (entry == null)
                break;
            if (hashTable[index].objectName.equals(objectName)) {
                hashTable[index] = null;
                status --;
                break;
            }
            index ++;
            loopMax ++;
        }
    }

    void removeAll() {
        for (int i = 0; i < hashTable.length; i++) {
            if (hashTable[i] != null) {
                hashTable[i] = null;
                status --;
            }
        }
    }

    Entry search(String objectName) {
        Entry result = null;
        int index = hash(objectName);
        int loopMax = 0;
        while (loopMax < hashTable.length) {
            Entry entry = hashTable[index];
            if (entry == null)
                break;
            if (hashTable[index].objectName.equals(objectName)) {
                result = hashTable[index];
                break;
            }
            if (index == hashTable.length - 1)
                index = 0;
            index ++;
            loopMax ++;
        }
        return result;
    }

    void report(boolean showEmpty) {
        System.out.println("HashTable report:");
        if (isEmpty())
            System.out.println("Empty");
        else {
            for (int i = 0; i < hashTable.length; i++) {
                if (hashTable[i] != null) {
                    System.out.println("Slot " + i + " - Used.");
                }
                else if (showEmpty){
                    System.out.println("Slot " + i + " - Empyt");
                }
            }
        }
    }

    int getStatus() {
        return status;
    }

    boolean isFull() {
        return status == hashTable.length;
    }

    boolean isEmpty() {
        return status == 0;
    }
}
