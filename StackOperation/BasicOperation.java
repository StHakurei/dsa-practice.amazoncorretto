/*
 * This sample code will present Stack (ADT) basic operation by AmazonCorretto.
 * [Stack Operation]
 * 1. Push - Add element to the top.
 * 2. Pop - Get element from the top.
 * 3. Peek - Check element from the top.
 * 4. isFull - Check if stack is full.
 * 5. isEmpty - Check if stack is empty.
 *
 * @author  Galahad
 * @version 1.0
 * @since   2019-12-25
 */
public class BasicOperation {

    public static void main(String[] args) {
        System.out.println("DSA-Practice (AmazonCorretto) - Stack Operations.");
        System.out.println("Create a stack storage.");
        Stack sample = new Stack(10);
        System.out.println("Capacity: " + sample.size());
        for (int i = 0; i < 20; i++) {
            if (!sample.isFull()) {
                System.out.println("Action: Push " + i + " into storage.");
                sample.push(i);
                System.out.println("Current size: " + sample.status());
            }
            else {
                System.out.println("Stack is full.");
                break;
            }
        }
        System.out.println("Push finished.");
        System.out.println("Action: Peek the top element.");
        System.out.println("Element: " + sample.peek());
        System.out.println("Action: Pop element from the top until the stack is empty.");
        while (true) {
            if (!sample.isEmpty()) {
                System.out.println("Pop element: " + sample.pop());
                System.out.println("Current size: " + sample.status());
            }
            else {
                System.out.println("Stack is empty.");
                break;
            }
        }
    }
}

class Stack {

    private int[] innerList;
    private int topIndex;

    public Stack(int size) {
        innerList = new int[size];
        topIndex = -1;
    }

    /**
     * [Stack Operation]
     * Push - Add element to the top.
     */
    void push(int data) {
        if (!isFull()) {
            topIndex ++;
            innerList[topIndex] = data;
        }
        else {
            System.out.println("Error: Inventory is full.");
        }
    }

    /**
     * [Stack Operation]
     * Pop - Get element from the top.
     */
    int pop() {
        if (!isEmpty()) {
            int data = innerList[topIndex];
            topIndex --;
            return data;
        }
        else {
            System.out.println("Error: Inventory is empty.");
            return -1;
        }
    }

    /**
     * [Stack Operation]
     * Peek - Check element from the top.
     */
    int peek() {
        return innerList[topIndex];
    }

    /**
     * Get the size of stack.
     */
    int size() {
        return innerList.length;
    }

    /**
     * Get the current inventory status.
     */
    int status() {
        return topIndex + 1;
    }

    /**
     * [Stack Operation]
     * isFull - Check if stack is full.
     */
    boolean isFull() {
        return topIndex == innerList.length - 1;
    }

    /**
     * [Stack Operation]
     * Peek - Check element from the top.
     */
    boolean isEmpty() {
        return topIndex == -1;
    }
}
/* Output: (100% match)
DSA-Practice (AmazonCorretto) - Stack Operations.
Create a stack storage.
Capacity: 10
Action: Push 0 into storage.
Current size: 1
Action: Push 1 into storage.
Current size: 2
Action: Push 2 into storage.
Current size: 3
Action: Push 3 into storage.
Current size: 4
Action: Push 4 into storage.
Current size: 5
Action: Push 5 into storage.
Current size: 6
Action: Push 6 into storage.
Current size: 7
Action: Push 7 into storage.
Current size: 8
Action: Push 8 into storage.
Current size: 9
Action: Push 9 into storage.
Current size: 10
Stack is full.
Push finished.
Action: Peek the top element.
Element: 9
Action: Pop element from the top until the stack is empty.
Pop element: 9
Current size: 9
Pop element: 8
Current size: 8
Pop element: 7
Current size: 7
Pop element: 6
Current size: 6
Pop element: 5
Current size: 5
Pop element: 4
Current size: 4
Pop element: 3
Current size: 3
Pop element: 2
Current size: 2
Pop element: 1
Current size: 1
Pop element: 0
Current size: 0
Stack is empty.
*///:~
