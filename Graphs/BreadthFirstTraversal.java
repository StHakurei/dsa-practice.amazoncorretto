/*
 * Breadth First Search (BFS) algorithm traverses a graph in a breadthward motion
 * and uses a queue to store the next vertex to start a search, when a dead end
 * occurs in any iteration.
 *
 * @author Galahad
 * @version 1.0
 * @since 2020-2-2
 */
import java.util.LinkedList;
import java.util.Queue;

public class BreadthFirstTraversal {

    public static void main(String[] args) {
        System.out.println("DSA-Practice (AmazonCorretto) - Graphs: Breadth First Traversal.");
        Graph object = new Graph(8);
        System.out.println("Create a graph with 8 vertices: 'S' 'A' 'B' 'C' 'D' 'E' 'F' 'G'.");
        object.addVertex("S");
        object.addVertex("A");
        object.addVertex("B");
        object.addVertex("C");
        object.addVertex("D");
        object.addVertex("E");
        object.addVertex("F");
        object.addVertex("G");
        System.out.println("Show all vertices.");
        object.displayVertices();
        System.out.println("Set adjacency between vertices: S -> A, B, C");
        object.addEdge("S", "A");
        object.addEdge("S", "B");
        object.addEdge("S", "C");
        System.out.println("Set adjacency between vertices: A -> D, B -> E, C -> F");
        object.addEdge("A", "D");
        object.addEdge("B", "E");
        object.addEdge("C", "F");
        System.out.println("Set adjacency between vertices: G -> D, E, F");
        object.addEdge("G", "D");
        object.addEdge("G", "E");
        object.addEdge("G", "F");
        System.out.println("Show all edges.");
        object.displayEdges();

        BreadthFirstSearch objectBFS = new BreadthFirstSearch(object);
        System.out.println("Start Breadth First Search via Vertex: S");
        objectBFS.search("S");
    }
}

class Vertex {

    public String label;
    public boolean visited;

    public Vertex(String label) {
        this.label = label;
        visited = false;
    }
}

class Graph {

    private Vertex[] vertices;
    private int[][] edges;
    private int slot;
    private int size;

    public Graph(int size) {
        vertices = new Vertex[size];
        edges = new int[size][size];
        slot = 0;
        this.size = size;
    }

    void addVertex(String label) {
        if (!isFull()) {
            Vertex vertex = new Vertex(label);
            vertices[slot] = vertex;
            slot ++;
        }
        else
            System.out.println("Failed to add vertex due to full of slot.");
    }

    void addEdge(String from, String to) {
        if (contains(from) && contains(to)) {
            edges[getIndex(from)][getIndex(to)] = 1;
            edges[getIndex(to)][getIndex(from)] = 1;
        }
        else
            System.out.println("Failed to add edge due to invalied vertex");
    }

    void visitVertex(int code) {
        vertices[code].visited = true;
        System.out.println("Visit vertex: " + vertices[code].label);
    }

    boolean isVisited(int code) {
        return vertices[code].visited;
    }

    int[] findAdjacency(int code) {
        return edges[code];
    }

    int getIndex(String label) {
        for (int i = 0; i < size; i++) {
            if (vertices[i].label.equalsIgnoreCase(label))
                return i;
        }
        return -1;
    }

    String getlabel(int index) {
        return vertices[index].label;
    }

    boolean contains(String label) {
        for (Vertex vertex : vertices) {
            if (vertex.label.equalsIgnoreCase(label))
                return true;
        }
        return false;
    }

    boolean isFull() {
        return slot == size;
    }

    void displayVertices() {
        for (int i = 0; i < slot; i++) {
            System.out.println("Vertes: " + vertices[i].label + " Code: " + i + " Visited: " + vertices[i].visited);
        }
    }

    void displayEdges() {
        for (int from = 0; from < size; from++) {
            for (int to = 0; to < size; to++) {
                if (edges[from][to] == 1) {
                    System.out.println("Edge: " + vertices[from].label + "->" + vertices[to].label);
                }
            }
        }
    }
}

public class BreadthFirstSearch {

    private Graph graph;
    private Queue<Integer> queue;

    public BreadthFirstSearch(Graph graph) {
        this.graph = graph;
        queue = new LinkedList<Integer>();
    }

    void search(String startVertex) {
        if (graph.contains(startVertex)) {
            graph.visitVertex(graph.getIndex(startVertex));
            queue.add(graph.getIndex(startVertex));
            iterator();
        }
        else
            System.out.println("Failed to start DFS due to invalied vertex");
    }

    void iterator() {
        while (queue.peek() != null) {
            int startVertex = queue.poll();
            int vertexIndex = 0;
            for (int adjacent : graph.findAdjacency(startVertex)) {
                if (adjacent == 1) {
                    if (!graph.isVisited(vertexIndex)) {
                        graph.visitVertex(vertexIndex);
                        queue.add(vertexIndex);
                    }
                }
                vertexIndex ++;
            }
        }
    }
}
