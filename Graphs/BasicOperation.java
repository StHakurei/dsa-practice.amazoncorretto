/*
 * This sample code will perform basic graph operations which includes
 * add vertex, add edge and display vertex. Vertexs will be implement by
 * array and Edges will be implement via two-demensional array.
 *
 * @author Galahad
 * @version 1.0
 * @since 2020-1-30
 */
public class BasicOperation {

    public static void main(String[] args) {
        System.out.println("DSA-Practice (AmazonCorretto) - Graphs Basic.");
        Graphs object = new Graphs(5);
        System.out.println("Action: Create a graph and add 3 vertices: 'A' 'B' 'C'.");
        object.addVertex("A");
        object.addVertex("B");
        object.addVertex("C");
        System.out.println("Show all vertices.");
        object.displayVertices();
        System.out.println("Action: Add edge 'AB' and 'AC'.");
        object.addEdge("A", "B");
        object.addEdge("A", "C");
        System.out.println("Show all edges.");
        object.displayEdges();
    }

}

class Graphs {

    class Vertex {

        public String label;
        public boolean visited;

        public Vertex(String label) {
            this.label = label;
            visited = false;
        }
    }

    private Vertex[] vertices;
    private int[][] edges;
    private int slot;
    private int size;

    public Graphs(int size) {
        vertices = new Vertex[size];
        edges = new int[size][size];
        slot = 0;
        this.size = size;
    }

    void addVertex(String label) {
        if (!isFull()) {
            Vertex vertex = new Vertex(label);
            vertices[slot] = vertex;
            slot ++;
        }
        else
            System.out.println("Failed to add vertex due to full of slot.");
    }

    void addEdge(String from, String to) {
        if (contains(from) && contains(to)) {
            edges[getIndex(from)][getIndex(to)] = 1;
            edges[getIndex(to)][getIndex(from)] = 1;
        }
        else
            System.out.println("Failed to add edge due to invalied vertex");
    }

    void displayVertices() {
        for (int i = 0; i < slot; i++) {
            System.out.println("Vertes: " + vertices[i].label + " Code: " + i + " Visited: " + vertices[i].visited);
        }
    }

    void displayEdges() {
        for (int from = 0; from < size; from++) {
            for (int to = 0; to < size; to++) {
                if (edges[from][to] == 1) {
                    System.out.println("Edge: " + vertices[from].label + "->" + vertices[to].label);
                }
            }
        }
    }

    int getIndex(String label) {
        for (int i = 0; i < size; i++) {
            if (vertices[i].label.equalsIgnoreCase(label))
                return i;
        }
        return -1;
    }

    boolean contains(String label) {
        for (Vertex vertex : vertices) {
            if (vertex.label.equalsIgnoreCase(label))
                return true;
        }
        return false;
    }

    boolean isFull() {
        return slot == size;
    }
}
