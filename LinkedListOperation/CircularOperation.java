/*
 * The CircularOperation sample code implements several methods to perform following
 * operations.
 * [Singly Linked List as Circular - Insertion]
 * 1. Insert element to the tail.
 * 2. Insert element to the head.
 * 3. Insert element by given index.
 *
 * [Singly Linked List as Circular - Deletion]
 * 1. Remove the first element of list.
 * 2. Remove the last element of list.
 * 3. Remove element by a given index.
 * 4. Remove element by a given value.
 *
 * [Doubly Linked List as Circular - Insertion]
 * 1. Insert element to the tail.
 * 2. Insert element to the head.
 * 3. Insert element by given index.
 *
 * [Doubly Linked List as Circular - Deletion]
 * 1. Remove first element from the list.
 * 2. Remove last element from the list.
 * 3. Remove element by given index.
 * 4. Remove element by given value.
 *
 * @author  Galahad
 * @version 1.0
 * @since   2019-12-19
 */
public class CircularOperation {

    public static void main(String[] args) {
        System.out.println("DSA-Practice (AmazonCorretto) - Circular LinkedList Operations.");
        System.out.println("\n--- Simple LinkedList as Circular - Insertion ---");
        SimpleCircularList simpleList = new SimpleCircularList();
        System.out.println("Create a Simply Circular linked list contains element from number 0 to 10.");
        for (int i = 0; i <= 10; i++) {
            simpleList.add(i);
        }
        System.out.println("Length: " + simpleList.size());
        simpleList.output();

        System.out.println("Action: Add 100 to the head.");
        simpleList.addToFirst(100);
        System.out.println("Length: " + simpleList.size());
        simpleList.output();
        System.out.println("Action: Append 200 to the list.");
        simpleList.add(200);
        System.out.println("Length: " + simpleList.size());
        simpleList.output();
        System.out.println("Action: Insert element 255 to the index 5 of the list.");
        simpleList.insert(5, 255);
        System.out.println("Length: " + simpleList.size());
        simpleList.output();

        System.out.println("\n--- Simple LinkedList as Circular - Deletion ---");
        System.out.println("Action: Remove the first element.");
        simpleList.removeFirst();
        System.out.println("Length: " + simpleList.size());
        simpleList.output();
        System.out.println("Action: Remove the last element.");
        simpleList.removeLast();
        System.out.println("Length: " + simpleList.size());
        simpleList.output();
        System.out.println("Action: Remove element by index 4.");
        simpleList.removeByIndex(4);
        System.out.println("Length: " + simpleList.size());
        simpleList.output();
        System.out.println("Action: Remove elemetn which value is 10.");
        simpleList.removeByValue(10);
        System.out.println("Length: " + simpleList.size());
        simpleList.output();
        System.out.println("Action: Remove elemetn which value is 0.");
        simpleList.removeByValue(0);
        System.out.println("Length: " + simpleList.size());
        simpleList.output();

        System.out.println("\n=== Doubly LinkedList as Circular - Insertion ===");
        System.out.println("Create a sample doubly circular list contains number from 10 to 20.");
        DoublyCircularList doublyList = new DoublyCircularList();
        for (int i = 20; i >= 10; i--) {
            doublyList.addToFirst(i);
        }
        System.out.println("Length: " + doublyList.size());
        doublyList.output();

        System.out.println("Action: Add number 9 to the head.");
        doublyList.addToFirst(9);
        System.out.println("Length: " + doublyList.size());
        doublyList.output();
        System.out.println("Action: Add number 100 to the tail.");
        doublyList.add(100);
        System.out.println("Length: " + doublyList.size());
        doublyList.output();
        System.out.println("Action: Add number 101 to the tail.");
        doublyList.add(101);
        System.out.println("Length: " + doublyList.size());
        doublyList.output();
        System.out.println("Action: Insert number 255 by index 5.");
        doublyList.insert(5, 255);
        System.out.println("Length: " + doublyList.size());
        doublyList.output();

        System.out.println("\n=== Doubly LinkedList as Circular - Deletion ===");
        System.out.println("Action: Remove first element.");
        doublyList.removeFirst();
        System.out.println("Length: " + doublyList.size());
        doublyList.output();
        System.out.println("Action: Remove last element.");
        doublyList.removeLast();
        System.out.println("Length: " + doublyList.size());
        doublyList.output();
        System.out.println("Action: Remove element by value 255.");
        doublyList.remove(255);
        System.out.println("Length: " + doublyList.size());
        doublyList.output();
        System.out.println("Action: Remove element by index 5.");
        doublyList.removeByIndex(5);
        System.out.println("Length: " + doublyList.size());
        doublyList.output();

        System.out.println("\nOutput whole list by read previous element.");
        System.out.println("Length: " + doublyList.size());
        doublyList.reversePrint();
    }
}

class CircularNode {

    public int data;
    public CircularNode next;

    public CircularNode(int data) {
        this.data = data;
        this.next = null;
    }
}

class SimpleCircularList {

    public CircularNode head;
    public int size = 0;

    SimpleCircularList() {
        head = null;
    }

    /**
     * [Singly Linked List as Circular - Insertion]
     * Insert element to the head.
     */
    void addToFirst(int data) {
        CircularNode node = new CircularNode(data);
        node.next = head;
        head = node;
        CircularNode cursor = head;
        int anker = 0;
        while (anker < size) {
            cursor = cursor.next;
            anker ++;
        }
        cursor.next = node;
        size ++;
    }

    /**
     * [Singly Linked List as Circular - Insertion]
     * Insert element to the tail.
     */
    void add(int data) {
        if (size == 0) {
            addToFirst(data);
        }
        else {
            CircularNode node = new CircularNode(data);
            CircularNode cursor = head;
            node.next = cursor;
            int anker = 0;
            while (anker < size - 1) {
                cursor = cursor.next;
                anker ++;
            }
            cursor.next = node;
            size ++;
        }
    }

    /**
     * [Singly Linked List as Circular - Insertion]
     * Insert element by given index.
     */
    void insert(int index, int data) {
        if (index == 0) {
            addToFirst(data);
        }
        else if (index == size) {
            add(data);
        }
        else {
            CircularNode node = new CircularNode(data);
            CircularNode previous = head;
            CircularNode cursor = head;
            int anker = 0;
            while (anker < index) {
                previous = cursor;
                cursor = cursor.next;
                anker ++;
            }
            previous.next = node;
            node.next = cursor;
            size ++;
        }
    }

    /**
     * [Singly Linked List as Circular - Deletion]
     * Remove the first element of list.
     */
    void removeFirst() {
        CircularNode cursor = head;
        CircularNode newFirst = cursor.next;
        int anker = 0;
        while (anker < size -1) {
            cursor = cursor.next;
            anker ++;
        }
        cursor.next = newFirst;
        head = newFirst;
        size --;
    }

    /**
     * [Singly Linked List as Circular - Deletion]
     * Remove the last element of list.
     */
    void removeLast() {
        CircularNode cursor = head;
        CircularNode previous = cursor;
        int anker = 0;
        while (anker < size - 1) {
            previous = cursor;
            cursor = cursor.next;
            anker ++;
        }
        previous.next = head;
        size --;
    }

    /**
     * [Singly Linked List as Circular - Deletion]
     * Remove element by a given index.
     */
    void removeByIndex(int index) {
        CircularNode cursor = head;
        CircularNode previous = cursor;
        int anker = 0;
        while (anker < index) {
            previous = cursor;
            cursor = cursor.next;
            anker ++;
        }
        cursor = cursor.next;
        previous.next = cursor;
        size --;
    }

    /**
     * [Singly Linked List as Circular - Deletion]
     * Remove element by a given value.
     */
    void removeByValue(int value) {
        CircularNode cursor = head;
        CircularNode previous = cursor;
        int anker = 0;
        while (anker <= size) {
            if (cursor.data == value) {
                if (anker == 0)
                    removeFirst();
                else {
                    cursor = cursor.next;
                    previous.next = cursor;
                    size --;
                }
                break;
            }
            else {
                previous = cursor;
                cursor = cursor.next;
                anker ++;
            }
        }
    }

    /**
     * Get length of LinkedList.
     */
    int size() {
        return size;
    }

    /**
     * Print the whole content of LinkedList.
     * To prevent infinite output, this method will only print twice
     * linked list loop.
     */
    void output() {
        int count = 0;
        CircularNode cursor = head;
        System.out.print("Head");
        while (count < size * 2) {
            if (count == size) {
                System.out.print(" -> Tail -> Head");
            }
            System.out.print(" -> " + cursor.data);
            cursor = cursor.next;
            count ++;
        }
        System.out.print(" -> Tail\n");
    }
}

class DoublyCircularNode {

    public int data;
    public DoublyCircularNode next;
    public DoublyCircularNode prev;

    DoublyCircularNode(int data) {
        this.data = data;
        this.next = null;
        this.prev = null;
    }
}

class DoublyCircularList {

    public DoublyCircularNode head;
    public int size = 0;

    DoublyCircularList() {
        head = null;
    }

    /**
     * [Doubly Linked List as Circular - Insertion]
     * Insert element to the head.
     */
    void addToFirst(int data) {
        DoublyCircularNode node = new DoublyCircularNode(data);
        if (size == 0) {
            node.next = head;
            node.prev = null;
            head = node;
        }
        else {
            DoublyCircularNode cursor = head;
            node.next = cursor;
            cursor.prev = node;
            int anker = 0;
            while (anker < size - 1) {
                cursor = cursor.next;
                anker ++;
            }
            node.prev = cursor;
            cursor.next = node;
            head = node;
        }
        size ++;
    }

    /**
     * [Doubly Linked List as Circular - Insertion]
     * Insert element to the tail.
     */
    void add(int data) {
        if (size == 0) {
            addToFirst(data);
        }
        else {
            DoublyCircularNode node = new DoublyCircularNode(data);
            DoublyCircularNode cursor = head;
            cursor.prev = node;
            node.next = cursor;
            int anker = 0;
            while (anker < size - 1) {
                cursor = cursor.next;
                anker ++;
            }
            cursor.next = node;
            node.prev = cursor;
            size ++;
        }
    }

    /**
     * [Doubly Linked List as Circular - Insertion]
     * Insert element by given index.
     */
    void insert(int index, int data) {
        if (index == 0) {
            addToFirst(data);
        }
        else if (index == size - 1) {
            add(data);
        }
        else {
            DoublyCircularNode node = new DoublyCircularNode(data);
            DoublyCircularNode cursor = head;
            DoublyCircularNode previous = head;
            int anker = 0;
            while (anker < size) {
                if (anker == index) {
                    previous.next = node;
                    node.prev = previous;
                    node.next = cursor;
                    cursor.prev = node;
                    size ++;
                    break;
                }
                previous = cursor;
                cursor = cursor.next;
                anker ++;
            }
        }
    }

    /**
     * [Doubly Linked List as Circular - Deletion]
     * Remove the first element from the list.
     */
    void removeFirst() {
        DoublyCircularNode cursor = head;
        DoublyCircularNode newHead = head;
        newHead = newHead.next;
        int anker = 0;
        while (anker < size - 1) {
            cursor = cursor.next;
            anker ++;
        }
        head = newHead;
        cursor.next = newHead;
        newHead.prev = cursor;
        size --;
    }

    /**
     * [Doubly Linked List as Circular - Deletion]
     * Remove the last element from the list.
     */
    void removeLast() {
        DoublyCircularNode cursor = head;
        DoublyCircularNode theFirst = head;
        int anker = 0;
        while (anker < size - 2) {
            cursor = cursor.next;
            anker ++;
        }
        theFirst.prev = cursor;
        cursor.next = theFirst;
        size --;
    }

    /**
     * [Doubly Linked List as Circular - Deletion]
     * Remove the element by value.
     */
    void remove(int value) {
        DoublyCircularNode cursor = head;
        DoublyCircularNode previous = head;
        int anker = 0;
        while (anker < size) {
            if (cursor.data == value) {
                if (anker == 0) {
                    removeFirst();
                }
                else if (anker == size - 1) {
                    removeLast();
                }
                else {
                    cursor = cursor.next;
                    previous.next = cursor;
                    cursor.prev = previous;
                    size --;
                }
                break;
            }
            previous = cursor;
            cursor = cursor.next;
            anker ++;
        }
    }

    /**
     * [Doubly Linked List as Circular - Deletion]
     * Remove the element by index.
     */
    void removeByIndex(int index) {
        if (index == 0) {
            removeFirst();
        }
        else if (index == size - 1) {
            removeLast();
        }
        else {
            DoublyCircularNode cursor = head;
            DoublyCircularNode previous = head;
            int anker = 0;
            while (anker < size - 1) {
                if (anker == index) {
                    cursor = cursor.next;
                    cursor.prev = previous;
                    previous.next = cursor;
                    size --;
                    break;
                }
                previous = cursor;
                cursor = cursor.next;
                anker ++;
            }
        }
    }

    /**
     * Get length of LinkedList.
     */
    int size() {
        return size;
    }

    /**
     * Print the whole content of LinkedList.
     * To prevent infinite output, this method will only print twice
     * linked list loop.
     */
    void output() {
        int count = 0;
        System.out.print("Head");
        DoublyCircularNode cursor = head;
        while (count < size * 2) {
            if (count == size) {
                System.out.print(" -> Tail -> Head");
            }
            System.out.print("-> " + cursor.data);
            cursor = cursor.next;
            count ++;
        }
        System.out.print(" -> Tail\n");
    }

    /**
     * Print the whole content of LinkedList in reverse deriction.
     * To prevent infinite output, this method will only print twice
     * linked list loop.
     */
    void reversePrint() {
        int count = 0;
        System.out.print("Head");
        DoublyCircularNode cursor = head;
        while (count < size * 2) {
            if (count == size) {
                System.out.print(" -> Tail -> Head");
            }
            System.out.print("-> " + cursor.data);
            cursor = cursor.prev;
            count ++;
        }
        System.out.print(" -> Tail\n");
    }
}
/* Output: (100% match)
DSA-Practice (AmazonCorretto) - Circular LinkedList Operations.

--- Simple LinkedList as Circular - Insertion ---
Create a Simply Circular linked list contains element from number 0 to 10.
Length: 11
Head -> 0 -> 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> 10 -> Tail -> Head -> 0 -> 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> 10 -> Tail
Action: Add 100 to the head.
Length: 12
Head -> 100 -> 0 -> 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> 10 -> Tail -> Head -> 100 -> 0 -> 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> 10 -> Tail
Action: Append 200 to the list.
Length: 13
Head -> 100 -> 0 -> 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> 10 -> 200 -> Tail -> Head -> 100 -> 0 -> 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> 10 -> 200 -> Tail
Action: Insert element 255 to the index 5 of the list.
Length: 14
Head -> 100 -> 0 -> 1 -> 2 -> 3 -> 255 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> 10 -> 200 -> Tail -> Head -> 100 -> 0 -> 1 -> 2 -> 3 -> 255 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> 10 -> 200 -> Tail

--- Simple LinkedList as Circular - Deletion ---
Action: Remove the first element.
Length: 13
Head -> 0 -> 1 -> 2 -> 3 -> 255 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> 10 -> 200 -> Tail -> Head -> 0 -> 1 -> 2 -> 3 -> 255 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> 10 -> 200 -> Tail
Action: Remove the last element.
Length: 12
Head -> 0 -> 1 -> 2 -> 3 -> 255 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> 10 -> Tail -> Head -> 0 -> 1 -> 2 -> 3 -> 255 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> 10 -> Tail
Action: Remove element by index 4.
Length: 11
Head -> 0 -> 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> 10 -> Tail -> Head -> 0 -> 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> 10 -> Tail
Action: Remove elemetn which value is 10.
Length: 10
Head -> 0 -> 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> Tail -> Head -> 0 -> 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> Tail
Action: Remove elemetn which value is 0.
Length: 9
Head -> 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> Tail -> Head -> 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> Tail

=== Doubly LinkedList as Circular - Insertion ===
Create a sample doubly circular list contains number from 10 to 20.
Length: 11
Head-> 10-> 11-> 12-> 13-> 14-> 15-> 16-> 17-> 18-> 19-> 20 -> Tail -> Head-> 10-> 11-> 12-> 13-> 14-> 15-> 16-> 17-> 18-> 19-> 20 -> Tail
Action: Add number 9 to the head.
Length: 12
Head-> 9-> 10-> 11-> 12-> 13-> 14-> 15-> 16-> 17-> 18-> 19-> 20 -> Tail -> Head-> 9-> 10-> 11-> 12-> 13-> 14-> 15-> 16-> 17-> 18-> 19-> 20 -> Tail
Action: Add number 100 to the tail.
Length: 13
Head-> 9-> 10-> 11-> 12-> 13-> 14-> 15-> 16-> 17-> 18-> 19-> 20-> 100 -> Tail -> Head-> 9-> 10-> 11-> 12-> 13-> 14-> 15-> 16-> 17-> 18-> 19-> 20-> 100 -> Tail
Action: Add number 101 to the tail.
Length: 14
Head-> 9-> 10-> 11-> 12-> 13-> 14-> 15-> 16-> 17-> 18-> 19-> 20-> 100-> 101 -> Tail -> Head-> 9-> 10-> 11-> 12-> 13-> 14-> 15-> 16-> 17-> 18-> 19-> 20-> 100-> 101 -> Tail
Action: Insert number 255 by index 5.
Length: 15
Head-> 9-> 10-> 11-> 12-> 13-> 255-> 14-> 15-> 16-> 17-> 18-> 19-> 20-> 100-> 101 -> Tail -> Head-> 9-> 10-> 11-> 12-> 13-> 255-> 14-> 15-> 16-> 17-> 18-> 19-> 20-> 100-> 101 -> Tail

=== Doubly LinkedList as Circular - Deletion ===
Action: Remove first element.
Length: 14
Head-> 10-> 11-> 12-> 13-> 255-> 14-> 15-> 16-> 17-> 18-> 19-> 20-> 100-> 101 -> Tail -> Head-> 10-> 11-> 12-> 13-> 255-> 14-> 15-> 16-> 17-> 18-> 19-> 20-> 100-> 101 -> Tail
Action: Remove last element.
Length: 13
Head-> 10-> 11-> 12-> 13-> 255-> 14-> 15-> 16-> 17-> 18-> 19-> 20-> 100 -> Tail -> Head-> 10-> 11-> 12-> 13-> 255-> 14-> 15-> 16-> 17-> 18-> 19-> 20-> 100 -> Tail
Action: Remove element by value 255.
Length: 12
Head-> 10-> 11-> 12-> 13-> 14-> 15-> 16-> 17-> 18-> 19-> 20-> 100 -> Tail -> Head-> 10-> 11-> 12-> 13-> 14-> 15-> 16-> 17-> 18-> 19-> 20-> 100 -> Tail
Action: Remove element by index 5.
Length: 11
Head-> 10-> 11-> 12-> 13-> 14-> 16-> 17-> 18-> 19-> 20-> 100 -> Tail -> Head-> 10-> 11-> 12-> 13-> 14-> 16-> 17-> 18-> 19-> 20-> 100 -> Tail

Output whole list by read previous element.
Length: 11
Head-> 10-> 100-> 20-> 19-> 18-> 17-> 16-> 14-> 13-> 12-> 11 -> Tail -> Head-> 10-> 100-> 20-> 19-> 18-> 17-> 16-> 14-> 13-> 12-> 11 -> Tail
*///:~
