/*
 * The BasicOperation sample code implements several methods to perform following
 * operations.
 * [Singly Linked List - Insertion]
 * 1. Insert element to the tail.
 * 2. Insert element to the head.
 * 3. Insert element by given index.
 *
 * [Singly Linked List - Deletion]
 * 1. Remove the first element of list.
 * 2. Remove the last element of list.
 * 3. Remove element by a given index.
 * 4. Remove element by a given value.
 *
 * [Singly Linked List - Search]
 * 1. Search target element from the list and return its index.
 * 2. Check if the list contains target element.
 *
 * [Singly Linked List - Reverse]
 * 1. Reverse the whole Linked List.
 *
 * [Doubly Linked List - Insertion]
 * 1. Insert element to the tail.
 * 2. Insert element to the head.
 * 3. Insert element by given index.
 *
 * [Doubly Linked List - Deletion]
 * 1. Remove first element from the list.
 * 2. Remove last element from the list.
 * 3. Remove element by given index.
 * 4. Remove element by given value.
 *
 * [Doubly Linked List - Search]
 * 1. Search target element from the list and return its index.
 * 2. Check if the list contains target element.
 *
 * [Doubly Linked List - Reverse]
 * 1. Reverse the whole Linked List.
 *
 * @author  Galahad
 * @version 1.0
 * @since   2019-11-18
 */
public class BasicOperation {

    public static void main(String[] args) {
        System.out.println("DSA-Practice (AmazonCorretto) - LinkedList Operations.");

        System.out.println("\n--- Simple LinkedList - Insertion ---");
        System.out.println("Action: create a singly Linked List, and number 0 - 9 into the list.");
        SimpleLinkedList list = new SimpleLinkedList();
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }
        System.out.println("Length: " + list.size());
        list.output();
        System.out.println("Action: insert [100, 100] between 4 and 5 of the list.");
        list.insert(5, 100);
        list.insert(5, 100);
        System.out.println("Length: " + list.size());
        list.output();
        System.out.println("Action: insert 10 to the head.");
        list.addToFirst(10);
        System.out.println("Length: " + list.size());
        list.output();
        System.out.println("Action: insert 500 to the tail.");
        list.add(500);
        System.out.println("Length: " + list.size());
        list.output();

        System.out.println("\n--- Simple LinkedList - Deletion ---");
        System.out.println("Action: Remove the first element.");
        list.removeFirst();
        System.out.println("Length: " + list.size());
        list.output();
        System.out.println("Action: Remove the last element.");
        list.removeLast();
        System.out.println("Length: " + list.size());
        list.output();
        System.out.println("Action: Remove 100 from the list.");
        list.removeByValue(100);
        System.out.println("Length: " + list.size());
        list.output();
        System.out.println("Action: Remove the element of index 3 from the list.");
        list.removeByIndex(3);
        System.out.println("Length: " + list.size());
        list.output();

        System.out.println("\n--- Simple LinkedList - Search ---");
        list.output();
        System.out.println("Action: Search 7 in the list and get its index.");
        System.out.println("Index: " + list.search(7));
        System.out.println("Action: Check following elements if they are contained in the list.");
        System.out.println("Number 3 in the list: " + list.contains(3));
        System.out.println("Number 4 in the list: " + list.contains(4));
        System.out.println("Number 5 in the list: " + list.contains(5));
        System.out.println("Number 100 in the list: " + list.contains(100));

        System.out.println("\n--- Simple LinkedList - Reverse ---");
        System.out.println("Sample List:");
        list.output();
        System.out.println("Reversed List:");
        SimpleLinkedList reversed = list.reverse(list);
        reversed.output();

        System.out.println("\n=== Doubly LinkedList - Insertion ===");
        System.out.println("Action: create a doubly Linked List, and number 10 - 20 into the list.");
        DoublyLinkedList doublyList = new DoublyLinkedList();
        for (int i = 10; i <= 20; i++) {
            doublyList.add(i);
        }
        System.out.println("Length: " + doublyList.size());
        doublyList.output();
        System.out.println("Action: Insert 100 and 200 to the head.");
        doublyList.addToFirst(100);
        doublyList.addToFirst(200);
        System.out.println("Length: " + doublyList.size());
        doublyList.output();
        System.out.println("Action: Insert 255 and 256 to the tail.");
        doublyList.add(255);
        doublyList.add(256);
        System.out.println("Length: " + doublyList.size());
        doublyList.output();
        System.out.println("Action: Insert 21 to between the 20 and 255.");
        doublyList.addByIndex(21, 13);
        System.out.println("Length: " + doublyList.size());
        doublyList.output();

        System.out.println("\n=== Doubly LinkedList - Deletion ===");
        System.out.println("Action: Remove the first element.");
        doublyList.removeFirst();
        System.out.println("Length: " + doublyList.size());
        doublyList.output();
        System.out.println("Action: Remove the last element.");
        doublyList.removeLast();
        System.out.println("Length: " + doublyList.size());
        doublyList.output();
        System.out.println("Action: Remove the element by index 12.");
        doublyList.removeByIndex(12);
        System.out.println("Length: " + doublyList.size());
        doublyList.output();
        System.out.println("Action: Remove 20 by value");
        doublyList.removeByValue(20);
        System.out.println("Length: " + doublyList.size());
        doublyList.output();

        System.out.println("\n=== Doubly LinkedList - Search ===");
        System.out.println("Action: Search 100 from the list.");
        int index0 = doublyList.search(100);
        if (index0 != -1) {
            System.out.println("True and its index: " + index0);
        }
        else {
            System.out.println("False.");
        }
        System.out.println("Action: Search 200 from the list.");
        int index1 = doublyList.search(200);
        if (index1 != -1) {
            System.out.println("True and its index: " + index1);
        }
        else {
            System.out.println("False.");
        }
        System.out.println("Action: Search 16 from the list.");
        int index2 = doublyList.search(16);
        if (index2 != -1) {
            System.out.println("True and its index: " + index2);
        }
        else {
            System.out.println("False.");
        }
        System.out.println("Action: Check if the list contains number 10");
        System.out.println(doublyList.contains(10));
        System.out.println("Action: Check if the list contains number 20");
        System.out.println(doublyList.contains(20));
        System.out.println("Action: Check if the list contains number 255");
        System.out.println(doublyList.contains(255));

        System.out.println("\n=== Doubly LinkedList - Reverse ===");
        System.out.println("Original List:");
        doublyList.output();
        DoublyLinkedList reversedDoublyList = doublyList.reverse(doublyList);
        System.out.println("Reversed List:");
        reversedDoublyList.output();
    }
}

class Node {

    public int data;
    public Node next;

    public Node(int data) {
        this.data = data;
        this.next = null;
    }
}

class SimpleLinkedList {

    public Node head;
    public int size = 0;

    SimpleLinkedList() {
        head = null;
    }

    /**
     * [Singly Linked List insertion.]
     * Insert element to the head.
     */
    void addToFirst(int data) {
        Node node = new Node(data);
        node.next = head;
        head = node;
        size ++;
    }

    /**
     * [Singly Linked List insertion.]
     * Insert element to the tail.
     */
    void add(int data) {
        if (head == null) {
            addToFirst(data);
        }
        else {
            Node node = new Node(data);
            Node cursor = head;
            while (cursor.next != null) {
                cursor = cursor.next;
            }
            cursor.next = node;
            size ++;
        }
    }

    /**
     * [Singly Linked List insertion]
     * Insert element by given index.
     */
    void insert(int index, int data) {
        Node node = new Node(data);
        Node cursor = head;
        int anker = 0;

        if (index == 0) {
            addToFirst(data);
            size ++;
        }
        else {
            while (anker < index - 1) {
                cursor = cursor.next;
                anker ++;
            }
            node.next = cursor.next;
            cursor.next = node;
            size ++;
        }
    }

    /**
     * [Singly Linked List Deletion]
     * Remove the first element of the list.
     */
    void removeFirst() {
        Node cursor = head;
        head = cursor.next;
        cursor.next = null;
        size --;
    }

    /**
     * [Singly Linked List Deletion]
     * Remove the last element of the list.
     */
    void removeLast() {
        Node cursor = head;
        int anker = 0;
        while (anker < size - 2) {
            cursor = cursor.next;
            anker ++;
        }
        cursor.next = null;
        size --;
    }

    /**
     * [Singly Linked List Deletion]
     * Remove element by a given index.
     */
    void removeByIndex(int index) {
        if (index == 0) {
            removeFirst();
        }
        else if (index == size - 2) {
            removeLast();
        }
        else {
            Node cursor = head;
            Node previous = head;
            int anker = 0;
            while (cursor != null) {
                if (anker == index) {
                    previous.next = cursor.next;
                    size --;
                    break;
                }
                previous = cursor;
                cursor = cursor.next;
                anker ++;
            }
        }
    }


    /**
     * [Singly Linked List Deletion]
     * Remove element(s) by a given value.
     */
    void removeByValue(int data) {
        Node cursor = head;
        Node previous = head;
        int anker = 0;
        while (cursor != null) {
            if (cursor.data == data) {
                if (anker == 0) {
                    removeFirst();
                }
                else if (anker == size - 2) {
                    removeLast();
                }
                else {
                    previous.next = cursor.next;
                    size --;
                }
            }
            else {
                previous = cursor;
            }
            cursor = cursor.next;
            anker ++;
        }
    }

    /**
     * [Singly Linked List Search]
     * Search target element from the list and return its index.
     */
    int search(int data) {
        Node cursor = head;
        int index = -1;
        int anker = 0;
        while (cursor != null) {
            if (cursor.data == data) {
                index = anker;
                break;
            }
            else {
                cursor = cursor.next;
                anker ++;
            }
        }
        return index;
    }

    /**
     * [Singly Linked List Search]
     * Check if the list contains target element.
     */
    boolean contains(int data) {
        return (search(data) != -1);
    }

    /**
     * [Singly Linked List Reverse]
     * Reverse the whole Linked List.
     */
    SimpleLinkedList reverse(SimpleLinkedList list) {
        SimpleLinkedList reverseList = new SimpleLinkedList();
        Node cursor = head;
        while (cursor != null) {
            reverseList.addToFirst(cursor.data);
            cursor = cursor.next;
        }
        return reverseList;
    }

    /**
     * Get length of LinkedList.
     */
    int size() {
        return size;
    }

    /**
     * Print the whole content of LinkedList.
     */
    void output() {
        System.out.print("Head");
        Node cursor = head;
        while (cursor != null) {
            System.out.print(" -> " + cursor.data);
            cursor = cursor.next;
        }
        System.out.print("-> Tail\n");
    }
}

class DoublyNode {
    public int data;
    public DoublyNode next;
    public DoublyNode prev;

    public DoublyNode(int data) {
        this.data = data;
        this.next = null;
        this.prev = null;
    }
}

class DoublyLinkedList {

    public DoublyNode head;
    public int size = 0;

    public DoublyLinkedList() {
        head = null;
    }

    /**
     * [Doubly Linked List - Insertion]
     * Insert element to the head.
     */
    void addToFirst(int data) {
        DoublyNode node = new DoublyNode(data);
        node.next = head;
        head = node;
        size ++;
        DoublyNode cursor = head;
        if (cursor.next != null) {
            cursor = cursor.next;
            cursor.prev = node;
        }
    }

    /**
     * [Doubly Linked List - Insertion]
     * Insert element to the tail.
     */
    void add(int data) {
        if (head == null) {
            addToFirst(data);
        }
        else {
            DoublyNode node = new DoublyNode(data);
            DoublyNode cursor = head;
            while (cursor.next != null) {
                cursor = cursor.next;
            }
            cursor.next = node;
            node.prev = cursor;
            size ++;
        }
    }

    /**
     * [Doubly Linked List - Insertion]
     * Insert element by given index.
     */
    void addByIndex(int data, int index) {
        if (index == 0) {
            addToFirst(data);
        }
        else {
            DoublyNode node = new DoublyNode(data);
            DoublyNode cursor = head;
            DoublyNode previous = head;
            int anker = 0;
            while (cursor != null) {
                if (anker == index) {
                    previous.next = node;
                    node.prev = previous;
                    node.next = cursor;
                    cursor.prev = node;
                    size ++;
                    break;
                }
                previous = cursor;
                cursor = cursor.next;
                anker ++;
            }
        }
    }

    /**
     * [Doubly Linked List - Deletion]
     * Remove first element from the list.
     */
    void removeFirst() {
        DoublyNode cursor = head;
        cursor = cursor.next;
        cursor.prev = null;
        head = cursor;
        size --;
    }

    /**
     * [Doubly Linked List - Deletion]
     * Remove last element from the list.
     */
    void removeLast() {
        DoublyNode cursor = head;
        DoublyNode previous = head;
        while (cursor.next != null) {
            previous = cursor;
            cursor = cursor.next;
        }
        previous.next = null;
        cursor.prev = null;
        size --;
    }

    /**
     * [Doubly Linked List - Deletion]
     * Remove element by given index.
     */
    void removeByIndex(int index) {
        if (index == 0) {
            removeFirst();
        }
        else if (index == size - 1) {
            removeLast();
        }
        else {
            DoublyNode cursor = head;
            DoublyNode previous = head;
            int anker = 0;
            while (cursor.next != null) {
                if (anker == index) {
                    cursor = cursor.next;
                    previous.next = cursor;
                    cursor.prev = previous;
                    size --;
                    break;
                }
                previous = cursor;
                cursor = cursor.next;
                anker ++;
            }
        }
    }

    /**
     * [Doubly Linked List - Deletion]
     * Remove element by given value.
     */
    void removeByValue(int value) {
        DoublyNode cursor = head;
        DoublyNode previous = head;
        int anker = 0;
        while (cursor != null) {
            if (cursor.data == value) {
                if (anker == 0) {
                    removeFirst();
                }
                else if (anker == size - 1) {
                    removeLast();
                }
                else {
                    cursor = cursor.next;
                    previous.next = cursor;
                    cursor.prev = previous;
                    size --;
                }
                break;
            }
            previous = cursor;
            cursor = cursor.next;
            anker ++;
        }
    }

    /**
     * [Doubly Linked List - Search]
     * Search target element from the list and return its index.
     */
    int search(int element) {
        DoublyNode cursor = head;
        int index = -1;
        int anker = 0;
        while (cursor != null) {
            if (cursor.data == element) {
                index = anker;
                break;
            }
            anker ++;
            cursor = cursor.next;
        }
        return index;
    }

    /**
     * [Doubly Linked List - Search]
     * Check if the list contains target element.
     */
    boolean contains(int element) {
        DoublyNode cursor = head;
        while (cursor != null) {
            if (cursor.data == element) {
                return true;
            }
            cursor = cursor.next;
        }
        return false;
    }

    /**
     * Get length of LinkedList.
     */
    int size() {
        return size;
    }

    /**
     * Print the whole content of LinkedList.
     */
    void output() {
        System.out.print("Head");
        DoublyNode cursor = head;
        while (cursor != null) {
            System.out.print(" -> " + cursor.data);
            cursor = cursor.next;
        }
        System.out.print(" -> Tail\n");
    }

    /**
     * [Doubly Linked List - Reverse]
     * Print the whole content of LinkedList in a reverse format.
     */
    DoublyLinkedList reverse(DoublyLinkedList list) {
        DoublyLinkedList reverseList = new DoublyLinkedList();
        DoublyNode cursor = head;
        while (cursor != null) {
            reverseList.addToFirst(cursor.data);
            cursor = cursor.next;
        }
        return reverseList;
    }
}
/* Output: (100% match)
DSA-Practice (AmazonCorretto) - LinkedList Operations.

--- Simple LinkedList - Insertion ---
Action: create a singly Linked List, and number 0 - 9 into the list.
Length: 10
Head -> 0 -> 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9-> Tail
Action: insert [100, 100] between 4 and 5 of the list.
Length: 12
Head -> 0 -> 1 -> 2 -> 3 -> 4 -> 100 -> 100 -> 5 -> 6 -> 7 -> 8 -> 9-> Tail
Action: insert 10 to the head.
Length: 13
Head -> 10 -> 0 -> 1 -> 2 -> 3 -> 4 -> 100 -> 100 -> 5 -> 6 -> 7 -> 8 -> 9-> Tail
Action: insert 500 to the tail.
Length: 14
Head -> 10 -> 0 -> 1 -> 2 -> 3 -> 4 -> 100 -> 100 -> 5 -> 6 -> 7 -> 8 -> 9 -> 500-> Tail

--- Simple LinkedList - Deletion ---
Action: Remove the first element.
Length: 13
Head -> 0 -> 1 -> 2 -> 3 -> 4 -> 100 -> 100 -> 5 -> 6 -> 7 -> 8 -> 9 -> 500-> Tail
Action: Remove the last element.
Length: 12
Head -> 0 -> 1 -> 2 -> 3 -> 4 -> 100 -> 100 -> 5 -> 6 -> 7 -> 8 -> 9-> Tail
Action: Remove 100 from the list.
Length: 10
Head -> 0 -> 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9-> Tail
Action: Remove the element of index 3 from the list.
Length: 9
Head -> 0 -> 1 -> 2 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9-> Tail

--- Simple LinkedList - Search ---
Head -> 0 -> 1 -> 2 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9-> Tail
Action: Search 7 in the list and get its index.
Index: 6
Action: Check following elements if they are contained in the list.
Number 3 in the list: false
Number 4 in the list: true
Number 5 in the list: true
Number 100 in the list: false

--- Simple LinkedList - Reverse ---
Sample List:
Head -> 0 -> 1 -> 2 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9-> Tail
Reversed List:
Head -> 9 -> 8 -> 7 -> 6 -> 5 -> 4 -> 2 -> 1 -> 0-> Tail

=== Doubly LinkedList - Insertion ===
Action: create a doubly Linked List, and number 10 - 20 into the list.
Length: 11
Head -> 10 -> 11 -> 12 -> 13 -> 14 -> 15 -> 16 -> 17 -> 18 -> 19 -> 20 -> Tail
Action: Insert 100 and 200 to the head.
Length: 13
Head -> 200 -> 100 -> 10 -> 11 -> 12 -> 13 -> 14 -> 15 -> 16 -> 17 -> 18 -> 19 -> 20 -> Tail
Action: Insert 255 and 256 to the tail.
Length: 15
Head -> 200 -> 100 -> 10 -> 11 -> 12 -> 13 -> 14 -> 15 -> 16 -> 17 -> 18 -> 19 -> 20 -> 255 -> 256 -> Tail
Action: Insert 21 to between the 20 and 255.
Length: 16
Head -> 200 -> 100 -> 10 -> 11 -> 12 -> 13 -> 14 -> 15 -> 16 -> 17 -> 18 -> 19 -> 20 -> 21 -> 255 -> 256 -> Tail

=== Doubly LinkedList - Deletion ===
Action: Remove the first element.
Length: 15
Head -> 100 -> 10 -> 11 -> 12 -> 13 -> 14 -> 15 -> 16 -> 17 -> 18 -> 19 -> 20 -> 21 -> 255 -> 256 -> Tail
Action: Remove the last element.
Length: 14
Head -> 100 -> 10 -> 11 -> 12 -> 13 -> 14 -> 15 -> 16 -> 17 -> 18 -> 19 -> 20 -> 21 -> 255 -> Tail
Action: Remove the element by index 12.
Length: 13
Head -> 100 -> 10 -> 11 -> 12 -> 13 -> 14 -> 15 -> 16 -> 17 -> 18 -> 19 -> 20 -> 255 -> Tail
Action: Remove 20 by value
Length: 12
Head -> 100 -> 10 -> 11 -> 12 -> 13 -> 14 -> 15 -> 16 -> 17 -> 18 -> 19 -> 255 -> Tail

=== Doubly LinkedList - Search ===
Action: Search 100 from the list.
True and its index: 0
Action: Search 200 from the list.
False.
Action: Search 16 from the list.
True and its index: 7
Action: Check if the list contains number 10
true
Action: Check if the list contains number 20
false
Action: Check if the list contains number 255
true

=== Doubly LinkedList - Reverse ===
Original List:
Head -> 100 -> 10 -> 11 -> 12 -> 13 -> 14 -> 15 -> 16 -> 17 -> 18 -> 19 -> 255 -> Tail
Reversed List:
Head -> 255 -> 19 -> 18 -> 17 -> 16 -> 15 -> 14 -> 13 -> 12 -> 11 -> 10 -> 100 -> Tail
*///:~
