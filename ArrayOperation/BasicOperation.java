/*
 * The BasicOperation sample code implements several method to perform following
 * operations.
 * [Array Insertion]
 * 1. Insert element(s) into a given position.
 * 2. Insert element(s) after the given position
 * 3. Insert element(s) before the given position.
 *
 * [Array Deletion]
 * 1. Remove an element from target array by index.
 * 2. Remove an element from target array.
 *
 * [Array Update]
 * 1. Update element(s) in an array.
 * 2. Update element in an array by given index.
 *
 * [Array Search]
 * 1. Search element in an array by value.
 * 2. Search element(s) in an array by a given range.
 *
 * @author  Galahad
 * @version 1.0
 * @since   2019-11-18
 */
import java.util.Arrays;

public class BasicOperation {

    /**
     * [Array Insertion]
     * Insert an element into a given postion.
     */
    private int[] insertElement(int[] array, int index, int element) {
        int[] newArray = new int[array.length + 1];
        int cursor = 0;
        while (cursor < index) {
            newArray[cursor] = array[cursor];
            cursor ++;
        }
        newArray[cursor] = element;
        cursor ++;
        while (cursor < newArray.length) {
            newArray[cursor] = array[cursor - 1];
            cursor ++;
        }
        return newArray;
    }

    /**
     * [Array Insertion]
     * Insert an element after the given index.
     */
    private int[] insertElementAfter(int[] array, int index, int element) {
        return insertElement(array, index + 1, element);
    }

    /**
     * [Array Insertion]
     * Insert an element before the given index.
     */
    private int[] insertElementBefore(int[] array, int index, int element) {
        return insertElement(array, index - 1, element);
    }

    /**
     * [Array Insertion]
     * Insert elements into a given postion.
     */
    private int[] mergeArray(int[] array, int index, int[] addedArray) {
        int[] newArray = new int[array.length + addedArray.length];
        int cursor = 0;
        int offset = 0;
        while (cursor < index) {
            newArray[cursor] = array[cursor];
            cursor ++;
        }
        for (int element : addedArray) {
            newArray[cursor] = element;
            cursor ++;
            offset ++;
        }
        while (cursor < newArray.length) {
            newArray[cursor] = array[cursor - offset];
            cursor ++;
        }
        return newArray;
    }

    /**
     * [Array Insertion]
     * Insert elements after the given index.
     */
    private int[] mergeArrayAfter(int[] array, int index, int[] addedArray) {
        return mergeArray(array, index + 1, addedArray);
    }

    /**
     * [Array Insertion]
     * Insert elements after the given index.
     */
    private int[] mergeArrayBefore(int[] array, int index, int[] addedArray) {
        return mergeArray(array, index - 1, addedArray);
    }

    /**
     * [Array Deletion]
     * Remove an element by given index.
     */
    private int[] removeElementByIndex(int[] array, int index) {
        int[] newArray = new int[array.length - 1];
        for (int i = 0; i < array.length; i++) {
            if (i < index)
                newArray[i] = array[i];

            if (i > index)
                newArray[i - 1] = array[i];
        }
        return newArray;
    }

    /**
     * [Array Deletion]
     * Remove an element from target array.
     */
    private int[] removeElement(int[] array, int element) {
        int index = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == element)
                index = i;
        }
        if (index == -1)
            return array;
        else
            return removeElementByIndex(array, index);
    }

    /**
     * [Array Update]
     * Update element(s) in an array.
     * This method will update all matched elements.
     */
    private int[] updateElement(int[] array, int target, int replace) {
        int[] newArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            if (array[i] == target)
                newArray[i] = replace;
            else
                newArray[i] = array[i];
        }
        return newArray;
    }

    /**
     * [Array Update]
     * Update element in an array by given index.
     */
    private int[] updateElementByIndex(int[] array, int index, int replace) {
        int[] newArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            if (i == index)
                newArray[i] = replace;
            else
                newArray[i] = array[i];
        }
        return newArray;
    }

    /**
     * [Array Search]
     * Search element by value.
     */
    private int searchElement(int[] array, int element) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == element) {
                return i;
            }
        }
        return -1;
    }

    /**
     * [Array Search]
     * Search element(s) which greater than given value.
     */
    private int[] searchElementGreaterThan(int[] array, int condition) {
        int[] bufferArray = new int[array.length];
        int offset = 0;
        for (int i = 0; i <array.length; i++) {
            if (array[i] > condition) {
                bufferArray[offset] = array[i];
                offset ++;
            }
        }
        int[] newArray = new int[offset];
        for (int i = 0; i < offset; i++) {
            newArray[i] = bufferArray[i];
        }
        return newArray;
    }

    /**
     * [Array Search]
     * Search element(s) which less than given value.
     */
    private int[] searchElementLessThan(int[] array, int condition) {
        int[] bufferArray = new int[array.length];
        int offset = 0;
        for (int i = 0; i <array.length; i++) {
            if (array[i] < condition) {
                bufferArray[offset] = array[i];
                offset ++;
            }
        }
        int[] newArray = new int[offset];
        for (int i = 0; i < offset; i++) {
            newArray[i] = bufferArray[i];
        }
        return newArray;
    }

    /**
     * Output content of an array in fixed format with a given label.
     */
    private void output(int[] array, String label) {
        System.out.println("[" + label + "]");
        System.out.println(Arrays.toString(array));
    }

    public static void main(String[] args) {
        BasicOperation object = new BasicOperation();
        System.out.println("DSA-Practice (AmazonCorretto) - Array Operations.");
        int[] originArray = {1, 2, 3, 5, 8, 13, 21};
        System.out.println("-- Single element insertion --");
        object.output(originArray, "Origin Array");
        System.out.println("Action: add 100 between 2 and 3.");
        object.output(object.insertElement(originArray, 2, 100), "After insertion");
        System.out.println("Action: add 255 after the index 2 of the Origin Array.");
        object.output(object.insertElementAfter(originArray, 2, 255), "After insertion.");
        System.out.println("Action: add 255 before the index 2 of the Origin Array.");
        object.output(object.insertElementBefore(originArray, 2, 255), "After insertion.");

        System.out.println("\n-- Multiple elements insertion --");
        object.output(originArray, "Origin Array");
        System.out.println("Action: add [100, 200, 300] between the 2 and 3 of the Origin Array");
        int[] anotherArray = {100, 200, 300};
        object.output(object.mergeArray(originArray, 2, anotherArray), "After insertion");
        System.out.println("Action: add [100, 200, 300] after the index 2 of the Origin Array");
        object.output(object.mergeArrayAfter(originArray, 2, anotherArray), "After insertion");
        System.out.println("Action: add [100, 200, 300] before the index 2 of the Origin Array");
        object.output(object.mergeArrayBefore(originArray, 2, anotherArray), "After insertion");

        System.out.println("\n-- Element deletion --");
        object.output(originArray, "Origin Array");
        System.out.println("Action: remove 3 between 2 and 5.");
        object.output(object.removeElementByIndex(originArray, 2), "After deletion");
        System.out.println("Action: remove 8 from Origin Array.");
        object.output(object.removeElement(originArray, 8), "After deletion");

        System.out.println("\n-- Array update --");
        object.output(originArray, "Origin Array");
        System.out.println("Action: update 21 by 100.");
        object.output(object.updateElement(originArray, 21, 100), "After update");
        System.out.println("Action: update the element in the index 3 of Origin Array by 100.");
        object.output(object.updateElementByIndex(originArray, 3, 100), "After update");

        System.out.println("\n-- Array search --");
        object.output(originArray, "Origin Array");
        System.out.println("Action: search 21 in array.");
        int result = object.searchElement(originArray, 21);
        if (result != -1)
            System.out.println("Found element, index: " + result);
        else
            System.out.println("No result.");
        System.out.println("Action: search 100 in array.");
        result = object.searchElement(originArray, 100);
        if (result != -1)
            System.out.println("Found element, index: " + result);
        else
            System.out.println("No result.");
        System.out.println("Action: search elements > 5.");
        object.output(object.searchElementGreaterThan(originArray, 5), "Search result");
        System.out.println("Action: search elements < 19.");
        object.output(object.searchElementLessThan(originArray, 19), "Search result");
    }
}
/* Output: (100% match)
DSA-Practice (AmazonCorrectto) - Array Operations.
-- Single element insertion --
[Origin Array]
[1, 2, 3, 5, 8, 13, 21]
Action: add 100 between 2 and 3.
[After insertion]
[1, 2, 100, 3, 5, 8, 13, 21]
Action: add 255 after the index 2 of the Origin Array.
[After insertion.]
[1, 2, 3, 255, 5, 8, 13, 21]
Action: add 255 before the index 2 of the Origin Array.
[After insertion.]
[1, 255, 2, 3, 5, 8, 13, 21]

-- Multiple elements insertion --
[Origin Array]
[1, 2, 3, 5, 8, 13, 21]
Action: add [100, 200, 300] between the 2 and 3 of the Origin Array
[After insertion]
[1, 2, 100, 200, 300, 3, 5, 8, 13, 21]
Action: add [100, 200, 300] after the index 2 of the Origin Array
[After insertion]
[1, 2, 3, 100, 200, 300, 5, 8, 13, 21]
Action: add [100, 200, 300] before the index 2 of the Origin Array
[After insertion]
[1, 100, 200, 300, 2, 3, 5, 8, 13, 21]

-- Element deletion --
[Origin Array]
[1, 2, 3, 5, 8, 13, 21]
Action: remove 3 between 2 and 5.
[After deletion]
[1, 2, 5, 8, 13, 21]
Action: remove 8 from Origin Array.
[After deletion]
[1, 2, 3, 5, 13, 21]

-- Array update --
[Origin Array]
[1, 2, 3, 5, 8, 13, 21]
Action: update 21 by 100.
[After update]
[1, 2, 3, 5, 8, 13, 100]
Action: update the element in the index 3 of Origin Array by 100.
[After update]
[1, 2, 3, 100, 8, 13, 21]

-- Array search --
[Origin Array]
[1, 2, 3, 5, 8, 13, 21]
Action: search 21 in array.
Found element, index: 6
Action: search 100 in array.
No result.
Action: search elements > 5.
[Search result]
[8, 13, 21]
Action: search elements < 19.
[Search result]
[1, 2, 3, 5, 8, 13]
*///:~
